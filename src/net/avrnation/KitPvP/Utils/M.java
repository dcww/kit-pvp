package net.avrnation.KitPvP.Utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

public class M {

	public static ItemStack HealthSplashI(){
		Potion potion = new Potion(PotionType.INSTANT_HEAL, 1);
		potion.splash();
		ItemStack potionstack = potion.toItemStack(1);
		return potionstack;
	}
	
	public static ItemStack HealthSplashII(){
		Potion potion = new Potion(PotionType.INSTANT_HEAL, 2);
		potion.splash();
		ItemStack potionstack = potion.toItemStack(1);
		return potionstack;
	}
	
	public static ItemStack SpeedI(){
		Potion potion = new Potion(PotionType.SPEED, 1);
		ItemStack potionstack = potion.toItemStack(1);
		return potionstack;
	}
	
	public static ItemStack SpeedII(){
		Potion potion = new Potion(PotionType.SPEED, 2);
		ItemStack potionstack = potion.toItemStack(1);
		return potionstack;
	}
	
	public static ItemStack StrengthI(){
		Potion potion = new Potion(PotionType.STRENGTH, 1);
		ItemStack potionstack = potion.toItemStack(1);
		return potionstack;
	}
	
	public static ItemStack StrengthII(){
		Potion potion = new Potion(PotionType.STRENGTH, 2);
		ItemStack potionstack = potion.toItemStack(1);
		return potionstack;
	}
	
	public static ItemStack RegenI(){
		Potion potion = new Potion(PotionType.REGEN, 1);
		ItemStack potionstack = potion.toItemStack(1);
		return potionstack;
	}
	
	public static ItemStack RegenII(){
		Potion potion = new Potion(PotionType.REGEN, 2);
		ItemStack potionstack = potion.toItemStack(1);
		return potionstack;
	}
	
	public static ItemStack FireProt(){
		Potion potion = new Potion(PotionType.FIRE_RESISTANCE, 1);
		ItemStack potionstack = potion.toItemStack(1);
		return potionstack;
	}
	
	public static ItemStack Enchant(){
		ItemStack itemstack = new ItemStack(384);
		return itemstack;
	}
	
	public static ItemStack Arrow(){
		ItemStack itemstack = new ItemStack(Material.ARROW);
		return itemstack;
	}
	
	public static ItemStack Bow(){
		ItemStack itemstack = new ItemStack(Material.BOW);
		return itemstack;
	}
}
