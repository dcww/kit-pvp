package net.avrnation.KitPvP.Utils;

import org.bukkit.ChatColor;

public class C {

	public static ChatColor Aqua = ChatColor.AQUA;
	public static ChatColor Black = ChatColor.BLACK;
	public static ChatColor Blue = ChatColor.BLUE;
	public static ChatColor AquaD = ChatColor.DARK_AQUA;
	public static ChatColor BlueD = ChatColor.DARK_BLUE;
	public static ChatColor GrayD = ChatColor.DARK_GRAY;
	public static ChatColor GreenD = ChatColor.DARK_GREEN;
	public static ChatColor PurpleD = ChatColor.DARK_PURPLE;
	public static ChatColor RedD = ChatColor.DARK_RED;
	public static ChatColor Gold = ChatColor.GOLD;
	public static ChatColor Gray = ChatColor.GRAY;
	public static ChatColor Green = ChatColor.GREEN;
	public static ChatColor PurpleL = ChatColor.LIGHT_PURPLE;
	public static ChatColor Red = ChatColor.RED;
	public static ChatColor White = ChatColor.WHITE;
	public static ChatColor Yellow = ChatColor.YELLOW;
	
	public static ChatColor Bold = ChatColor.BOLD;
	public static ChatColor Italic = ChatColor.ITALIC;
	public static ChatColor Strike = ChatColor.STRIKETHROUGH;
	public static ChatColor Magic = ChatColor.MAGIC;
	public static ChatColor Reset = ChatColor.RESET;
	public static ChatColor Underline = ChatColor.UNDERLINE;
	
	public static String AquaBold = (Aqua + "" + Bold);
	public static String BlackBold = (Aqua + "" + Bold + "");
	public static String BlueBold = (Aqua + "" + Bold + "");
	public static String AquaDBold = (Aqua + "" + Bold + "");
	public static String BlueDBold = (BlueD + "" + Bold);
	public static String GrayDBold = (Aqua + "" + Bold + "");
	public static String GreenDBold = (Aqua + "" + Bold + "");
	public static String PurpleDBold = (Aqua + "" + Bold + "");
	public static String RedDBold = (Aqua + "" + Bold + "");
	public static String GoldBold = (Gold + "" + Bold);
	public static String GrayBold = (Aqua + "" + Bold + "");
	public static String GreenBold = (Aqua + "" + Bold + "");
	public static String PurpleLBold = (Aqua + "" + Bold + "");
	public static String RedBold = (Aqua + "" + Bold + "");
	public static String WhiteBold = (Aqua + "" + Bold + "");
	public static String YellowBold = (Aqua + "" + Bold + "");
	
	
}
