package net.avrnation.KitPvP;

import net.avrnation.KitPvP.Handlers.Kits.KitsFull.IronKits;
import net.avrnation.KitPvP.Handlers.MySQL.MySQLFunctions;
import net.avrnation.KitPvP.Object.HexaPlayer;
import net.avrnation.KitPvP.Utils.C;
import net.avrnation.KitPvP.Utils.MessageManager;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class KitPvPCommandExecutor implements CommandExecutor {

	public static boolean isInt(String sInt) {
		try {
			Integer.parseInt(sInt);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}
	
	public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args) {
		
		Player player = (Player) sender;
		
		if(commandLabel.equalsIgnoreCase("pvp")){
			if(args.length > 0){
				if(args[0].equalsIgnoreCase("check")){
					if(args.length > 1){
						if(!(args[1].isEmpty())){
							//Player target = Global.getHexaPlayer(args[1].toLowerCase());//Bukkit.getServer().getPlayer(args[1]);
							HexaPlayer hexatarget = null;
							try {
								hexatarget = Global.getHexaPlayer(args[1].toLowerCase());
							} catch (Exceptions e) {
								e.printStackTrace();
							}
							if (!(hexatarget == null)) {
								sender.sendMessage(C.GoldBold + hexatarget.getName() + " has " + hexatarget.getPoints() + " Points!");
								//hexatarget.getPoints();
								hexatarget.SaveNow();
							} else {
								sender.sendMessage(C.RedBold + args[1] + " does not seem to exist in our Databases! Please recheck the players name!");
							}
						} else {
							sender.sendMessage(MessageManager.Error + "Shouldn't ever get to this message!");
						}
					} else {
						HexaPlayer hexa = null;
						try {
							hexa = Global.getHexaPlayer(sender.getName());
						} catch (Exceptions e1) {
							e1.printStackTrace();
						}
						
						if (!(hexa == null)) {
							//hexa.getPoints();
							try {
								Global.getPlayer(sender.getName()).sendMessage(C.GoldBold + "You have " + hexa.getPoints() + " Points!");
							} catch (Exceptions e) {
								sender.sendMessage(MessageManager.Error + "A horrible Error has occured. Please contact a Developer Immediatly, with the command typed!");
								e.printStackTrace();
							}
							hexa.SaveNow();
						} else {
							sender.sendMessage(MessageManager.Error + "A horrible Error has occured. Please contact a Developer Immediatly, with the command typed!");
						}
					}
				} else if(args[0].equalsIgnoreCase("give")){
					if(player.isOp()){
						if(args.length > 1){
							if(!(args[1].isEmpty())){
								Player target = Bukkit.getServer().getPlayer(args[1]);
								if(MySQLFunctions.ContainsPlayerName(target)){
									if(args.length > 2){
										if(!(args[2].isEmpty())){
											HexaPlayer hexa = Global.getHexaPlayer(target);
											int additionalPoints = Integer.parseInt(args[2]);
											hexa.setPoints(hexa.getPoints() + additionalPoints);
											hexa.SaveNow();
											if(player.isOnline()){
												try {
													player.sendMessage(C.GoldBold + Global.getPlayer(hexa).getName() + " now has " + (hexa.getPoints()) + " Points!");
													Global.getPlayer(hexa).sendMessage(C.AquaBold + player.getName() + " has just given you " + additionalPoints + " Points!");
												} catch (Exceptions e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
											} else {
												player.sendMessage(C.GoldBold + player.getName() + " is not currently online. They will be notified when they log on!");
												//TODO Notification Of Point Change Code
											}
											//MySQLFunctions.GivePoints(target, player, args[2]); int additionalPoints = Integer.parseInt(amount);
										}
									} else {
										player.sendMessage(MessageManager.Error + "Please enter a Point Amount!");
									}
								} else {
									player.sendMessage(MessageManager.Error + args[1] + " does not exist in our Databases! Please recheck the players name!");
								}
							} else {
								player.sendMessage(MessageManager.Error + "Please enter a player name!");
							}
						}
					} else {
						player.sendMessage(MessageManager.NoPermission);
					}
				} else if(args[0].equalsIgnoreCase("take")){
					if(player.isOp()){
						if(args.length > 1){
							if(!(args[1].isEmpty())){
								Player target = Bukkit.getServer().getPlayer(args[1]);
								if(MySQLFunctions.ContainsPlayerName(target)){
									if(args.length > 2){
										if(!(args[2].isEmpty())){
											MySQLFunctions.TakePoints(target, player, args[2]);
										}
									} else {
										player.sendMessage(MessageManager.Error + "Please enter a Point Amount!");
									}
								} else {
									player.sendMessage(MessageManager.Error + args[1] + " does not exist in our Databases! Please recheck the players name!");
								}
							} else {
								player.sendMessage(MessageManager.Error + "Please enter a player name!");
							}
						}
					} else {
						player.sendMessage(MessageManager.NoPermission);
					}
				} else if(args[0].equalsIgnoreCase("set")){
					//TODO Add Set Command - Sets players Tokens!
					IronKits.Teir4(player);
				} else if(args[0].equalsIgnoreCase("reset")){
					//TODO Add Reset Command - Resets players Tokens!
				} else if(args[0].equalsIgnoreCase("hexaplayers")){
					player.sendMessage("Hexaplayers Array List: " + Global.hexaplayers);
				} else {
					
				}
			} else {
				//TODO Add Help Menu
			}
		}
		return false;
	}
}
