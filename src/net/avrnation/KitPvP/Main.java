package net.avrnation.KitPvP;

import java.sql.SQLException;

import net.avrnation.KitPvP.Handlers.InventoryGUI.InventoryGUIManager;
import net.avrnation.KitPvP.Handlers.InventoryGUI.KitShopHandlers;
import net.avrnation.KitPvP.Handlers.Kits.KitManager;
import net.avrnation.KitPvP.Handlers.Player.PlayerChat;
import net.avrnation.KitPvP.Handlers.Player.PlayerJoin;
import net.avrnation.KitPvP.Handlers.Player.PlayerKill;
import net.avrnation.KitPvP.Handlers.Scoreboard.ScoreboardTimer;
import net.avrnation.KitPvP.Handlers.SignHandler.SignAction;
import net.avrnation.KitPvP.Handlers.SignHandler.SignCreator;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	private KitPvPCommandExecutor CommandExecutor;
	
	public static Plugin instance;
	
	public void onEnable() {
		getLogger().info("Hexa KitPvP has been enabled.");
		
		Bukkit.getServer().getPluginManager().registerEvents(new SignCreator(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new SignAction(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new InventoryGUIManager(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new KitShopHandlers(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerKill(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerJoin(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerChat(), this);
		//Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new ScoreboardTimer(), 100L, 100L);
		
		CommandExecutor = new KitPvPCommandExecutor();
		getCommand("pvp").setExecutor(CommandExecutor);
		
		instance = this;
		
		try {
			Global.loadHexaPlayers();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		KitManager.LoadKits();
	}

	public void onDisable() {
		getLogger().info("Hexa KitPvP has been disabled.");
	}
	
	public static Plugin getInstance(){
		return instance;
	}
	
}
