package net.avrnation.KitPvP.Handlers.SignHandler;

import net.avrnation.KitPvP.Main;
import net.avrnation.KitPvP.Handlers.InventoryGUI.MainInventory;
import net.avrnation.KitPvP.Handlers.MySQL.MySQLFunctions;
import net.avrnation.KitPvP.Utils.C;
import net.avrnation.KitPvP.Utils.M;
import net.avrnation.KitPvP.Utils.MessageManager;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class SignAction implements Listener {

	private static String line1;
	private static String line2;
	private static String line3;
	private static int cost;
	private static int amount;
	
	@EventHandler
	public static void onSignAction(PlayerInteractEvent event){
		final Player player = event.getPlayer();
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (event.getClickedBlock().getType() == Material.WALL_SIGN || event.getClickedBlock().getType() == Material.SIGN_POST) {
				//if(player.getItemInHand().getType() == Material.AIR){
					Sign sign = (Sign) event.getClickedBlock().getState();
					
					if (sign.getLine(0).equalsIgnoreCase(C.GoldBold + "[Kits]")) {
						Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
							  public void run() {
								  MainInventory.OpenGUI(player);
							  }
						}, 2L);
					} else if (sign.getLine(0).equalsIgnoreCase(C.GoldBold + "[Shop]")) {
						if(sign.getLine(1).isEmpty()){
							return;
						}
						line1 = sign.getLine(1);
						if(sign.getLine(2).isEmpty()){
							return;
						}
						line2 = sign.getLine(2);
						if(sign.getLine(3).isEmpty()){
							return;
						}
						line3 = sign.getLine(3).replace("$", "");
						amount = Integer.parseInt(line2);
						cost = Integer.parseInt(line3);
						if(line1.equalsIgnoreCase("healthi")){
							if(MySQLFunctions.CheckPoints(player, cost)){
								if(CheckInventory(player, amount)){
									MySQLFunctions.TakePoints(player, cost, line1);
									for(int x = 0; x < amount; x++){
										player.getInventory().addItem(M.HealthSplashI());
									}
									player.updateInventory();
								}
							} else {
								player.sendMessage(MessageManager.Error + "You do not have enough Points!");
							}
						} else if(line1.equalsIgnoreCase("healthii")){
							if(MySQLFunctions.CheckPoints(player, cost)){
								if(CheckInventory(player, amount)){
									MySQLFunctions.TakePoints(player, cost, line1);
									for(int x = 0; x < amount; x++){
										player.getInventory().addItem(M.HealthSplashII());
									}
									player.updateInventory();
								}
							} else {
								player.sendMessage(MessageManager.Error + "You do not have enough Points!");
							}
						} else if(line1.equalsIgnoreCase("speedi")){
							if(MySQLFunctions.CheckPoints(player, cost)){
								if(CheckInventory(player, amount)){
									MySQLFunctions.TakePoints(player, cost, line1);
									for(int x = 0; x < amount; x++){
										player.getInventory().addItem(M.SpeedI());
									}
									player.updateInventory();
								}
							} else {
								player.sendMessage(MessageManager.Error + "You do not have enough Points!");
							}
						} else if(line1.equalsIgnoreCase("speedii")){
							if(MySQLFunctions.CheckPoints(player, cost)){
								if(CheckInventory(player, amount)){
									MySQLFunctions.TakePoints(player, cost, line1);
									for(int x = 0; x < amount; x++){
										player.getInventory().addItem(M.SpeedII());
									}
									player.updateInventory();
								}
							} else {
								player.sendMessage(MessageManager.Error + "You do not have enough Points!");
							}
						} else if(line1.equalsIgnoreCase("strengthi")){
							if(MySQLFunctions.CheckPoints(player, cost)){
								if(CheckInventory(player, amount)){
									MySQLFunctions.TakePoints(player, cost, line1);
									for(int x = 0; x < amount; x++){
										player.getInventory().addItem(M.StrengthI());
									}
									player.updateInventory();
								}
							} else {
								player.sendMessage(MessageManager.Error + "You do not have enough Points!");
							}
						} else if(line1.equalsIgnoreCase("strengthii")){
							if(MySQLFunctions.CheckPoints(player, cost)){
								if(CheckInventory(player, amount)){
									MySQLFunctions.TakePoints(player, cost, line1);
									for(int x = 0; x < amount; x++){
										player.getInventory().addItem(M.StrengthII());
									}
									player.updateInventory();
								}
							} else {
								player.sendMessage(MessageManager.Error + "You do not have enough Points!");
							}
						} else if(line1.equalsIgnoreCase("regeni")){
							if(MySQLFunctions.CheckPoints(player, cost)){
								if(CheckInventory(player, amount)){
									MySQLFunctions.TakePoints(player, cost, line1);
									for(int x = 0; x < amount; x++){
										player.getInventory().addItem(M.RegenI());
									}
									player.updateInventory();
								}
							} else {
								player.sendMessage(MessageManager.Error + "You do not have enough Points!");
							}
						} else if(line1.equalsIgnoreCase("regenii")){
							if(MySQLFunctions.CheckPoints(player, cost)){
								if(CheckInventory(player, amount)){
									MySQLFunctions.TakePoints(player, cost, line1);
									for(int x = 0; x < amount; x++){
										player.getInventory().addItem(M.RegenII());
									}
									player.updateInventory();
								}
							} else {
								player.sendMessage(MessageManager.Error + "You do not have enough Points!");
							}
						} else if(line1.equalsIgnoreCase("fireres")){
							if(MySQLFunctions.CheckPoints(player, cost)){
								if(CheckInventory(player, amount)){
									MySQLFunctions.TakePoints(player, cost, line1);
									for(int x = 0; x < amount; x++){
										player.getInventory().addItem(M.FireProt());
									}
									player.updateInventory();
								}
							} else {
								player.sendMessage(MessageManager.Error + "You do not have enough Points!");
							}
						} else if(line1.equalsIgnoreCase("enchant")){
							if(MySQLFunctions.CheckPoints(player, cost)){
								if(CheckInventory(player, amount)){
									MySQLFunctions.TakePoints(player, cost, line1);
									for(int x = 0; x < amount; x++){
										player.getInventory().addItem(M.FireProt());
									}
									player.updateInventory();
								}
							} else {
								player.sendMessage(MessageManager.Error + "You do not have enough Points!");
							}
						} else if(line1.equalsIgnoreCase("fireres")){
							if(MySQLFunctions.CheckPoints(player, cost)){
								if(CheckInventory(player, amount)){
									MySQLFunctions.TakePoints(player, cost, line1);
									for(int x = 0; x < amount; x++){
										player.getInventory().addItem(M.FireProt());
									}
									player.updateInventory();
								}
							} else {
								player.sendMessage(MessageManager.Error + "You do not have enough Points!");
							}
						} else if(line1.equalsIgnoreCase("fireres")){
							if(MySQLFunctions.CheckPoints(player, cost)){
								if(CheckInventory(player, amount)){
									MySQLFunctions.TakePoints(player, cost, line1);
									for(int x = 0; x < amount; x++){
										player.getInventory().addItem(M.FireProt());
									}
									player.updateInventory();
								}
							} else {
								player.sendMessage(MessageManager.Error + "You do not have enough Points!");
							}
						} else {
							player.sendMessage(MessageManager.Error + "Debug not added yet");
						}
					}
					
				//}
			}
		}
	}
	
	public static boolean CheckInventory(Player player, int amount){
		int count = 0;
		for (int i = 0; i < 36; i++) {
			if(player.getInventory().getItem(i) == null){
				count++;
			}
		}
		if(count >= amount){
			return true;
		} else {
			return false;
		}
	}
}
