package net.avrnation.KitPvP.Handlers.SignHandler;

import net.avrnation.KitPvP.Utils.C;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

public class SignCreator implements Listener {

	private static String line1;
	private static String line2;
	private static String line3;
	
	@EventHandler
	public static void onSignCreation(SignChangeEvent event){
		
		Player player = event.getPlayer();
		
		if(event.getLine(0).equalsIgnoreCase("[Kits]")){
			if(player.isOp()){
				event.setLine(0, (C.GoldBold + "[Kits]"));
				event.setLine(1, (ChatColor.GREEN + "Click Here To"));
				event.setLine(2, (ChatColor.GREEN + "Select A Kit"));
				event.setLine(3, "");
			} else {
				player.sendMessage(C.RedBold + "You do not have the required Permissions to do this!");
			}
		} else if(event.getLine(0).equalsIgnoreCase("[Shop]")){
			if(player.isOp()){
				if(event.getLine(1).isEmpty()){
					return;
				}
				line1 = event.getLine(1);
				if(event.getLine(2).isEmpty()){
					return;
				}
				line2 = event.getLine(2);
				if(event.getLine(3).isEmpty()){
					return;
				}
				line3 = event.getLine(3);
				
				event.setLine(0, (C.GoldBold + "[Shop]"));
			} else {
				player.sendMessage(C.RedBold + "You do not have the required Permissions to do this!");
			}
		}
	}
}
