package net.avrnation.KitPvP.Handlers.Kits;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

public class Leather {

	//
	//Tier One
	//
	public static ItemStack LeatherTier1Helmet(){
		ItemStack LeatherTier1Helmet = new ItemStack(Material.LEATHER_HELMET);
		LeatherTier1Helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		return LeatherTier1Helmet;
	}
	
	public static ItemStack LeatherTier1Chest(){
		ItemStack LeatherTier1Chest = new ItemStack(Material.LEATHER_CHESTPLATE);
		LeatherTier1Chest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		return LeatherTier1Chest;
	}
	
	public static ItemStack LeatherTier1Leggings(){
		ItemStack LeatherTier1Leggings = new ItemStack(Material.LEATHER_LEGGINGS);
		LeatherTier1Leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		return LeatherTier1Leggings;
	}
	
	public static ItemStack LeatherTier1Boots(){
		ItemStack LeatherTier1Boots = new ItemStack(Material.LEATHER_BOOTS);
		LeatherTier1Boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		return LeatherTier1Boots;
	}
	
	public static ItemStack LeatherTier1Sword(){
		ItemStack LeatherTier1Sword = new ItemStack(Material.WOOD_SWORD);
		return LeatherTier1Sword;
	}
	
	//
	//Tier Two
	//
	public static ItemStack LeatherTier2Helmet(){
		ItemStack LeatherTier2Helmet = new ItemStack(Material.LEATHER_HELMET);
		LeatherTier2Helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
		return LeatherTier2Helmet;
	}
	
	public static ItemStack LeatherTier2Chest(){
		ItemStack LeatherTier2Chest = new ItemStack(Material.LEATHER_CHESTPLATE);
		LeatherTier2Chest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
		return LeatherTier2Chest;
	}
	
	public static ItemStack LeatherTier2Leggings(){
		ItemStack LeatherTier2Leggings = new ItemStack(Material.LEATHER_LEGGINGS);
		LeatherTier2Leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
		return LeatherTier2Leggings;
	}
	
	public static ItemStack LeatherTier2Boots(){
		ItemStack LeatherTier2Boots = new ItemStack(Material.LEATHER_BOOTS);
		LeatherTier2Boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
		return LeatherTier2Boots;
	}
	
	public static ItemStack LeatherTier2Sword(){
		ItemStack LeatherTier2Sword = new ItemStack(Material.WOOD_SWORD);
		LeatherTier2Sword.addEnchantment(Enchantment.DAMAGE_ALL, 1);
		return LeatherTier2Sword;
	}
	
	//
	//Tier Three
	//
	public static ItemStack LeatherTier3Helmet(){
		ItemStack LeatherTier3Helmet = new ItemStack(Material.LEATHER_HELMET);
		LeatherTier3Helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		return LeatherTier3Helmet;
	}
	
	public static ItemStack LeatherTier3Chest(){
		ItemStack LeatherTier3Chest = new ItemStack(Material.LEATHER_CHESTPLATE);
		LeatherTier3Chest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		return LeatherTier3Chest;
	}
	
	public static ItemStack LeatherTier3Leggings(){
		ItemStack LeatherTier3Leggings = new ItemStack(Material.LEATHER_LEGGINGS);
		LeatherTier3Leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		return LeatherTier3Leggings;
	}
	
	public static ItemStack LeatherTier3Boots(){
		ItemStack LeatherTier3Boots = new ItemStack(Material.LEATHER_BOOTS);
		LeatherTier3Boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		return LeatherTier3Boots;
	}
	
	public static ItemStack LeatherTier3Sword(){
		ItemStack LeatherTier3Sword = new ItemStack(Material.WOOD_SWORD);
		LeatherTier3Sword.addEnchantment(Enchantment.DAMAGE_ALL, 2);
		return LeatherTier3Sword;
	}
	
	//
	//Tier Four
	//
	public static ItemStack LeatherTier4Helmet(){
		ItemStack LeatherTier4Helmet = new ItemStack(Material.LEATHER_HELMET);
		LeatherTier4Helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		return LeatherTier4Helmet;
	}
	
	public static ItemStack LeatherTier4Chest(){
		ItemStack LeatherTier4Chest = new ItemStack(Material.LEATHER_CHESTPLATE);
		LeatherTier4Chest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		return LeatherTier4Chest;
	}
	
	public static ItemStack LeatherTier4Leggings(){
		ItemStack LeatherTier4Leggings = new ItemStack(Material.LEATHER_LEGGINGS);
		LeatherTier4Leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		return LeatherTier4Leggings;
	}
	
	public static ItemStack LeatherTier4Boots(){
		ItemStack LeatherTier4Boots = new ItemStack(Material.LEATHER_BOOTS);
		LeatherTier4Boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		return LeatherTier4Boots;
	}
	
	public static ItemStack LeatherTier4Sword(){
		ItemStack LeatherTier4Sword = new ItemStack(Material.WOOD_SWORD);
		LeatherTier4Sword.addEnchantment(Enchantment.DAMAGE_ALL, 3);
		return LeatherTier4Sword;
	}
}
