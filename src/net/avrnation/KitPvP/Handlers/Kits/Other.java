package net.avrnation.KitPvP.Handlers.Kits;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Other {

	//
	// Free Kit
	//
	public static ItemStack FreeHelmet(){
		ItemStack FreeHelmet = new ItemStack(Material.LEATHER_HELMET);
		return FreeHelmet;
	}
	
	public static ItemStack FreeChest(){
		ItemStack FreeChest = new ItemStack(Material.LEATHER_CHESTPLATE);
		return FreeChest;
	}
	
	public static ItemStack FreeLeggings(){
		ItemStack FreeLeggings = new ItemStack(Material.LEATHER_LEGGINGS);
		return FreeLeggings;
	}
	
	public static ItemStack FreeBoots(){
		ItemStack FreeBoots = new ItemStack(Material.LEATHER_BOOTS);
		return FreeBoots;
	}
	
	public static ItemStack FreeSword(){
		ItemStack FreeSword = new ItemStack(Material.WOOD_SWORD);
		return FreeSword;
	}
	
}
