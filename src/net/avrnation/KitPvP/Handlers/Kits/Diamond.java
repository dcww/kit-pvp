package net.avrnation.KitPvP.Handlers.Kits;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

public class Diamond {

	//
	//Tier One
	//
	public static ItemStack Tier1Helmet(){
		ItemStack DiamondTier1Helmet = new ItemStack(Material.DIAMOND_HELMET);
		DiamondTier1Helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		return DiamondTier1Helmet;
	}
	
	public static ItemStack Tier1Chest(){
		ItemStack DiamondTier1Chest = new ItemStack(Material.DIAMOND_CHESTPLATE);
		DiamondTier1Chest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		return DiamondTier1Chest;
	}
	
	public static ItemStack Tier1Leggings(){
		ItemStack DiamondTier1Leggings = new ItemStack(Material.DIAMOND_LEGGINGS);
		DiamondTier1Leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		return DiamondTier1Leggings;
	}
	
	public static ItemStack Tier1Boots(){
		ItemStack DiamondTier1Boots = new ItemStack(Material.DIAMOND_BOOTS);
		DiamondTier1Boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		return DiamondTier1Boots;
	}
	
	public static ItemStack Tier1Sword(){
		ItemStack DiamondTier1Sword = new ItemStack(Material.DIAMOND_SWORD);
		return DiamondTier1Sword;
	}
	//
	//Tier Two
	//
	public static ItemStack Tier2Helmet(){
		ItemStack DiamondTier2Helmet = new ItemStack(Material.DIAMOND_HELMET);
		DiamondTier2Helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
		return DiamondTier2Helmet;
	}
	
	public static ItemStack Tier2Chest(){
		ItemStack DiamondTier2Chest = new ItemStack(Material.DIAMOND_CHESTPLATE);
		DiamondTier2Chest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
		return DiamondTier2Chest;
	}
	
	public static ItemStack Tier2Leggings(){
		ItemStack DiamondTier2Leggings = new ItemStack(Material.DIAMOND_LEGGINGS);
		DiamondTier2Leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
		return DiamondTier2Leggings;
	}
	
	public static ItemStack Tier2Boots(){
		ItemStack DiamondTier2Boots = new ItemStack(Material.DIAMOND_BOOTS);
		DiamondTier2Boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
		return DiamondTier2Boots;
	}
	
	public static ItemStack Tier2Sword(){
		ItemStack DiamondTier2Sword = new ItemStack(Material.WOOD_SWORD);
		DiamondTier2Sword.addEnchantment(Enchantment.DAMAGE_ALL, 1);
		return DiamondTier2Sword;
	}
	
	//
	//Tier Three
	//
	public static ItemStack Tier3Helmet(){
		ItemStack DiamondTier3Helmet = new ItemStack(Material.DIAMOND_HELMET);
		DiamondTier3Helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		return DiamondTier3Helmet;
	}
	
	public static ItemStack Tier3Chest(){
		ItemStack DiamondTier3Chest = new ItemStack(Material.DIAMOND_CHESTPLATE);
		DiamondTier3Chest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		return DiamondTier3Chest;
	}
	
	public static ItemStack Tier3Leggings(){
		ItemStack DiamondTier3Leggings = new ItemStack(Material.DIAMOND_LEGGINGS);
		DiamondTier3Leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		return DiamondTier3Leggings;
	}
	
	public static ItemStack Tier3Boots(){
		ItemStack DiamondTier3Boots = new ItemStack(Material.DIAMOND_BOOTS);
		DiamondTier3Boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		return DiamondTier3Boots;
	}
	
	public static ItemStack Tier3Sword(){
		ItemStack DiamondTier3Sword = new ItemStack(Material.WOOD_SWORD);
		DiamondTier3Sword.addEnchantment(Enchantment.DAMAGE_ALL, 2);
		return DiamondTier3Sword;
	}
	
	//
	//Tier Four
	//
	public static ItemStack Tier4Helmet(){
		ItemStack DiamondTier4Helmet = new ItemStack(Material.DIAMOND_HELMET);
		DiamondTier4Helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		DiamondTier4Helmet.setAmount(1);
		return DiamondTier4Helmet;
	}
	
	public static ItemStack Tier4Chest(){
		ItemStack DiamondTier4Chest = new ItemStack(Material.DIAMOND_CHESTPLATE);
		DiamondTier4Chest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		DiamondTier4Chest.setAmount(1);
		return DiamondTier4Chest;
	}
	
	public static ItemStack Tier4Leggings(){
		ItemStack DiamondTier4Leggings = new ItemStack(Material.DIAMOND_LEGGINGS);
		DiamondTier4Leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		DiamondTier4Leggings.setAmount(1);
		return DiamondTier4Leggings;
	}
	
	public static ItemStack Tier4Boots(){
		ItemStack DiamondTier4Boots = new ItemStack(Material.DIAMOND_BOOTS);
		DiamondTier4Boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		DiamondTier4Boots.setAmount(1);
		return DiamondTier4Boots;
	}
	
	public static ItemStack Tier4Sword(){
		ItemStack DiamondTier4Sword = new ItemStack(Material.DIAMOND_SWORD);
		DiamondTier4Sword.addEnchantment(Enchantment.DAMAGE_ALL, 3);
		DiamondTier4Sword.setAmount(1);
		return DiamondTier4Sword;
	}
}
