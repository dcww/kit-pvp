package net.avrnation.KitPvP.Handlers.Kits.KitsFull;

import net.avrnation.KitPvP.Handlers.Kits.Other;

import org.bukkit.entity.Player;

public class OtherKits {

	public static void Free(Player player){
		player.getInventory().addItem(Other.FreeBoots());
		player.getInventory().addItem(Other.FreeLeggings());
		player.getInventory().addItem(Other.FreeChest());
		player.getInventory().addItem(Other.FreeHelmet());
		player.getInventory().addItem(Other.FreeSword());
	}
}
