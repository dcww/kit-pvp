package net.avrnation.KitPvP.Handlers.Kits.KitsFull;

import net.avrnation.KitPvP.Handlers.Kits.Leather;

import org.bukkit.entity.Player;

public class LeatherKits {

	public static void Teir1(Player player){
		player.getInventory().addItem(Leather.LeatherTier1Boots());
		player.getInventory().addItem(Leather.LeatherTier1Leggings());
		player.getInventory().addItem(Leather.LeatherTier1Chest());
		player.getInventory().addItem(Leather.LeatherTier1Helmet());
		player.getInventory().addItem(Leather.LeatherTier1Sword());
	}
	
	public static void Teir2(Player player){
		player.getInventory().addItem(Leather.LeatherTier2Boots());
		player.getInventory().addItem(Leather.LeatherTier2Leggings());
		player.getInventory().addItem(Leather.LeatherTier2Chest());
		player.getInventory().addItem(Leather.LeatherTier2Helmet());
		player.getInventory().addItem(Leather.LeatherTier2Sword());
	}
	
	public static void Teir3(Player player){
		player.getInventory().addItem(Leather.LeatherTier3Boots());
		player.getInventory().addItem(Leather.LeatherTier3Leggings());
		player.getInventory().addItem(Leather.LeatherTier3Chest());
		player.getInventory().addItem(Leather.LeatherTier3Helmet());
		player.getInventory().addItem(Leather.LeatherTier3Sword());
	}
	
	public static void Teir4(Player player){
		player.getInventory().addItem(Leather.LeatherTier4Boots());
		player.getInventory().addItem(Leather.LeatherTier4Leggings());
		player.getInventory().addItem(Leather.LeatherTier4Chest());
		player.getInventory().addItem(Leather.LeatherTier4Helmet());
		player.getInventory().addItem(Leather.LeatherTier4Sword());
	}
}
