package net.avrnation.KitPvP.Handlers.Kits.KitsFull;

import net.avrnation.KitPvP.Handlers.Kits.Diamond;

import org.bukkit.entity.Player;

public class DiamondKits {

	public static void Teir1(Player player){
		player.getInventory().addItem(Diamond.Tier1Boots());
		player.getInventory().addItem(Diamond.Tier1Leggings());
		player.getInventory().addItem(Diamond.Tier1Chest());
		player.getInventory().addItem(Diamond.Tier1Helmet());
		player.getInventory().addItem(Diamond.Tier1Sword());
	}
	
	public static void Teir2(Player player){
		player.getInventory().addItem(Diamond.Tier2Boots());
		player.getInventory().addItem(Diamond.Tier2Leggings());
		player.getInventory().addItem(Diamond.Tier2Chest());
		player.getInventory().addItem(Diamond.Tier2Helmet());
		player.getInventory().addItem(Diamond.Tier2Sword());
	}
	
	public static void Teir3(Player player){
		player.getInventory().addItem(Diamond.Tier3Boots());
		player.getInventory().addItem(Diamond.Tier3Leggings());
		player.getInventory().addItem(Diamond.Tier3Chest());
		player.getInventory().addItem(Diamond.Tier3Helmet());
		player.getInventory().addItem(Diamond.Tier3Sword());
	}
	
	public static void Teir4(Player player){
		player.getInventory().addItem(Diamond.Tier4Boots());
		player.getInventory().addItem(Diamond.Tier4Leggings());
		player.getInventory().addItem(Diamond.Tier4Chest());
		player.getInventory().addItem(Diamond.Tier4Helmet());
		player.getInventory().addItem(Diamond.Tier4Sword());
	}
}
