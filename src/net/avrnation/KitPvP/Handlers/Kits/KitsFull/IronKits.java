package net.avrnation.KitPvP.Handlers.Kits.KitsFull;

import net.avrnation.KitPvP.Handlers.Kits.Iron;

import org.bukkit.entity.Player;

public class IronKits {
	
	public static void Teir1(Player player){
		player.getInventory().addItem(Iron.Tier1Boots());
		player.getInventory().addItem(Iron.Tier1Leggings());
		player.getInventory().addItem(Iron.Tier1Chest());
		player.getInventory().addItem(Iron.Tier1Helmet());
		player.getInventory().addItem(Iron.Tier1Sword());
	}
	
	public static void Teir2(Player player){
		player.getInventory().addItem(Iron.Tier2Boots());
		player.getInventory().addItem(Iron.Tier2Leggings());
		player.getInventory().addItem(Iron.Tier2Chest());
		player.getInventory().addItem(Iron.Tier2Helmet());
		player.getInventory().addItem(Iron.Tier2Sword());
	}
	
	public static void Teir3(Player player){
		player.getInventory().addItem(Iron.Tier3Boots());
		player.getInventory().addItem(Iron.Tier3Leggings());
		player.getInventory().addItem(Iron.Tier3Chest());
		player.getInventory().addItem(Iron.Tier3Helmet());
		player.getInventory().addItem(Iron.Tier3Sword());
	}
	
	public static void Teir4(Player player){
		player.getInventory().addItem(Iron.Tier4Boots());
		player.getInventory().addItem(Iron.Tier4Leggings());
		player.getInventory().addItem(Iron.Tier4Chest());
		player.getInventory().addItem(Iron.Tier4Helmet());
		player.getInventory().addItem(Iron.Tier4Sword());
	}
}
