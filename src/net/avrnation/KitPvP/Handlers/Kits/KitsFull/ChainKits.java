package net.avrnation.KitPvP.Handlers.Kits.KitsFull;

import net.avrnation.KitPvP.Handlers.Kits.Chain;

import org.bukkit.entity.Player;

public class ChainKits {

	public static void Teir1(Player player){
		player.getInventory().addItem(Chain.Tier1Boots());
		player.getInventory().addItem(Chain.Tier1Leggings());
		player.getInventory().addItem(Chain.Tier1Chest());
		player.getInventory().addItem(Chain.Tier1Helmet());
		player.getInventory().addItem(Chain.Tier1Sword());
	}
	
	public static void Teir2(Player player){
		player.getInventory().addItem(Chain.Tier2Boots());
		player.getInventory().addItem(Chain.Tier2Leggings());
		player.getInventory().addItem(Chain.Tier2Chest());
		player.getInventory().addItem(Chain.Tier2Helmet());
		player.getInventory().addItem(Chain.Tier2Sword());
	}
	
	public static void Teir3(Player player){
		player.getInventory().addItem(Chain.Tier3Boots());
		player.getInventory().addItem(Chain.Tier3Leggings());
		player.getInventory().addItem(Chain.Tier3Chest());
		player.getInventory().addItem(Chain.Tier3Helmet());
		player.getInventory().addItem(Chain.Tier3Sword());
	}
	
	public static void Teir4(Player player){
		player.getInventory().addItem(Chain.Tier4Boots());
		player.getInventory().addItem(Chain.Tier4Leggings());
		player.getInventory().addItem(Chain.Tier4Chest());
		player.getInventory().addItem(Chain.Tier4Helmet());
		player.getInventory().addItem(Chain.Tier4Sword());
	}
}
