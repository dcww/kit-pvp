package net.avrnation.KitPvP.Handlers.Kits;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class KitManager {

	public static Map<String, Integer> kitcosts = new ConcurrentHashMap<String, Integer>();
	public static Map<String, Integer> unlockablecosts = new ConcurrentHashMap<String, Integer>();
	
	public static void LoadKits() {
		kitcosts.put("LTeir1Cost", LTeir1Cost);
		kitcosts.put("LTeir2Cost", LTeir2Cost);
		kitcosts.put("LTeir3Cost", LTeir3Cost);
		kitcosts.put("LTeir4Cost", LTeir4Cost);
		kitcosts.put("CTeir1Cost", CTeir1Cost);
		kitcosts.put("CTeir2Cost", CTeir2Cost);
		kitcosts.put("CTeir3Cost", CTeir3Cost);
		kitcosts.put("CTeir4Cost", CTeir4Cost);
		kitcosts.put("ITeir1Cost", ITeir1Cost);
		kitcosts.put("ITeir2Cost", ITeir2Cost);
		kitcosts.put("ITeir3Cost", ITeir3Cost);
		kitcosts.put("ITeir4Cost", ITeir4Cost);
		kitcosts.put("DTeir1Cost", DTeir1Cost);
		kitcosts.put("DTeir2Cost", DTeir2Cost);
		kitcosts.put("DTeir3Cost", DTeir3Cost);
		kitcosts.put("DTeir4Cost", DTeir4Cost);
		
		unlockablecosts.put("LTeir1Upgrade", LTeir1Upgrade);
		unlockablecosts.put("LTeir2Upgrade", LTeir2Upgrade);
		unlockablecosts.put("LTeir3Upgrade", LTeir3Upgrade);
		unlockablecosts.put("LTeir4Upgrade", LTeir4Upgrade);
		unlockablecosts.put("CTeir1Upgrade", CTeir1Upgrade);
		unlockablecosts.put("CTeir2Upgrade", CTeir2Upgrade);
		unlockablecosts.put("CTeir3Upgrade", CTeir3Upgrade);
		unlockablecosts.put("CTeir4Upgrade", CTeir4Upgrade);
		unlockablecosts.put("ITeir1Upgrade", ITeir1Upgrade);
		unlockablecosts.put("ITeir2Upgrade", ITeir2Upgrade);
		unlockablecosts.put("ITeir3Upgrade", ITeir3Upgrade);
		unlockablecosts.put("ITeir4Upgrade", ITeir4Upgrade);
		unlockablecosts.put("DTeir1Upgrade", DTeir1Upgrade);
		unlockablecosts.put("DTeir2Upgrade", DTeir2Upgrade);
		unlockablecosts.put("DTeir3Upgrade", DTeir3Upgrade);
		unlockablecosts.put("DTeir4Upgrade", DTeir4Upgrade);
	}
	
	public static int LTeir1Cost = 9;
	public static int LTeir1Upgrade = 3;
	
	public static int LTeir2Cost = 15;
	public static int LTeir2Upgrade = 6;
	
	public static int LTeir3Cost = 21;
	public static int LTeir3Upgrade = 9;
	
	public static int LTeir4Cost = 27;
	public static int LTeir4Upgrade = 12;
	
	public static int CTeir1Cost = 30;
	public static int CTeir1Upgrade = 9;
	
	public static int CTeir2Cost = 36;
	public static int CTeir2Upgrade = 15;
	
	public static int CTeir3Cost = 42;
	public static int CTeir3Upgrade = 21;
	
	public static int CTeir4Cost = 48;
	public static int CTeir4Upgrade = 27;
	
	public static int ITeir1Cost = 51;
	public static int ITeir1Upgrade = 33;
	
	public static int ITeir2Cost = 57;
	public static int ITeir2Upgrade = 39;
	
	public static int ITeir3Cost = 63;
	public static int ITeir3Upgrade = 45;
	
	public static int ITeir4Cost = 69;
	public static int ITeir4Upgrade = 51;
	
	public static int DTeir1Cost = 72;
	public static int DTeir1Upgrade = 57;
	
	public static int DTeir2Cost = 78;
	public static int DTeir2Upgrade = 63;
	
	public static int DTeir3Cost = 84;
	public static int DTeir3Upgrade = 69;
	
	public static int DTeir4Cost = 90;
	public static int DTeir4Upgrade = 75;
}
