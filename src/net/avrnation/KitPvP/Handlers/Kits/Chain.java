package net.avrnation.KitPvP.Handlers.Kits;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

public class Chain {

	//
	//Tier One
	//
	public static ItemStack Tier1Helmet(){
		ItemStack ChainTier1Helmet = new ItemStack(Material.CHAINMAIL_HELMET);
		ChainTier1Helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		return ChainTier1Helmet;
	}
	
	public static ItemStack Tier1Chest(){
		ItemStack ChainTier1Chest = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
		ChainTier1Chest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		return ChainTier1Chest;
	}
	
	public static ItemStack Tier1Leggings(){
		ItemStack ChainTier1Leggings = new ItemStack(Material.CHAINMAIL_LEGGINGS);
		ChainTier1Leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		return ChainTier1Leggings;
	}
	
	public static ItemStack Tier1Boots(){
		ItemStack ChainTier1Boots = new ItemStack(Material.CHAINMAIL_BOOTS);
		ChainTier1Boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		return ChainTier1Boots;
	}
	
	public static ItemStack Tier1Sword(){
		ItemStack ChainTier1Sword = new ItemStack(Material.STONE_SWORD);
		return ChainTier1Sword;
	}
	//
	//Tier Two
	//
	public static ItemStack Tier2Helmet(){
		ItemStack ChainTier2Helmet = new ItemStack(Material.CHAINMAIL_HELMET);
		ChainTier2Helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
		return ChainTier2Helmet;
	}
	
	public static ItemStack Tier2Chest(){
		ItemStack ChainTier2Chest = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
		ChainTier2Chest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
		return ChainTier2Chest;
	}
	
	public static ItemStack Tier2Leggings(){
		ItemStack ChainTier2Leggings = new ItemStack(Material.CHAINMAIL_LEGGINGS);
		ChainTier2Leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
		return ChainTier2Leggings;
	}
	
	public static ItemStack Tier2Boots(){
		ItemStack ChainTier2Boots = new ItemStack(Material.CHAINMAIL_BOOTS);
		ChainTier2Boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
		return ChainTier2Boots;
	}
	
	public static ItemStack Tier2Sword(){
		ItemStack ChainTier2Sword = new ItemStack(Material.STONE_SWORD);
		ChainTier2Sword.addEnchantment(Enchantment.DAMAGE_ALL, 1);
		return ChainTier2Sword;
	}
	
	//
	//Tier Three
	//
	public static ItemStack Tier3Helmet(){
		ItemStack ChainTier3Helmet = new ItemStack(Material.CHAINMAIL_HELMET);
		ChainTier3Helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		return ChainTier3Helmet;
	}
	
	public static ItemStack Tier3Chest(){
		ItemStack ChainTier3Chest = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
		ChainTier3Chest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		return ChainTier3Chest;
	}
	
	public static ItemStack Tier3Leggings(){
		ItemStack ChainTier3Leggings = new ItemStack(Material.CHAINMAIL_LEGGINGS);
		ChainTier3Leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		return ChainTier3Leggings;
	}
	
	public static ItemStack Tier3Boots(){
		ItemStack ChainTier3Boots = new ItemStack(Material.CHAINMAIL_BOOTS);
		ChainTier3Boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		return ChainTier3Boots;
	}
	
	public static ItemStack Tier3Sword(){
		ItemStack ChainTier3Sword = new ItemStack(Material.STONE_SWORD);
		ChainTier3Sword.addEnchantment(Enchantment.DAMAGE_ALL, 2);
		return ChainTier3Sword;
	}
	
	//
	//Tier Four
	//
	public static ItemStack Tier4Helmet(){
		ItemStack ChainTier4Helmet = new ItemStack(Material.CHAINMAIL_HELMET);
		ChainTier4Helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		ChainTier4Helmet.setAmount(1);
		return ChainTier4Helmet;
	}
	
	public static ItemStack Tier4Chest(){
		ItemStack ChainTier4Chest = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
		ChainTier4Chest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		ChainTier4Chest.setAmount(1);
		return ChainTier4Chest;
	}
	
	public static ItemStack Tier4Leggings(){
		ItemStack ChainTier4Leggings = new ItemStack(Material.CHAINMAIL_LEGGINGS);
		ChainTier4Leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		ChainTier4Leggings.setAmount(1);
		return ChainTier4Leggings;
	}
	
	public static ItemStack Tier4Boots(){
		ItemStack ChainTier4Boots = new ItemStack(Material.CHAINMAIL_BOOTS);
		ChainTier4Boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		ChainTier4Boots.setAmount(1);
		return ChainTier4Boots;
	}
	
	public static ItemStack Tier4Sword(){
		ItemStack ChainTier4Sword = new ItemStack(Material.STONE_SWORD);
		ChainTier4Sword.addEnchantment(Enchantment.DAMAGE_ALL, 3);
		ChainTier4Sword.setAmount(1);
		return ChainTier4Sword;
	}
}
