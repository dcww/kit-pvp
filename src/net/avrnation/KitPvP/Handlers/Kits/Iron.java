package net.avrnation.KitPvP.Handlers.Kits;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

public class Iron {

	//
	//Tier One
	//
	public static ItemStack Tier1Helmet(){
		ItemStack IronTier1Helmet = new ItemStack(Material.IRON_HELMET);
		IronTier1Helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		return IronTier1Helmet;
	}
	
	public static ItemStack Tier1Chest(){
		ItemStack IronTier1Chest = new ItemStack(Material.IRON_CHESTPLATE);
		IronTier1Chest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		return IronTier1Chest;
	}
	
	public static ItemStack Tier1Leggings(){
		ItemStack IronTier1Leggings = new ItemStack(Material.IRON_LEGGINGS);
		IronTier1Leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		return IronTier1Leggings;
	}
	
	public static ItemStack Tier1Boots(){
		ItemStack IronTier1Boots = new ItemStack(Material.IRON_BOOTS);
		IronTier1Boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		return IronTier1Boots;
	}
	
	public static ItemStack Tier1Sword(){
		ItemStack IronTier1Sword = new ItemStack(Material.IRON_SWORD);
		return IronTier1Sword;
	}
	//
	//Tier Two
	//
	public static ItemStack Tier2Helmet(){
		ItemStack IronTier2Helmet = new ItemStack(Material.IRON_HELMET);
		IronTier2Helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
		return IronTier2Helmet;
	}
	
	public static ItemStack Tier2Chest(){
		ItemStack IronTier2Chest = new ItemStack(Material.IRON_CHESTPLATE);
		IronTier2Chest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
		return IronTier2Chest;
	}
	
	public static ItemStack Tier2Leggings(){
		ItemStack IronTier2Leggings = new ItemStack(Material.IRON_LEGGINGS);
		IronTier2Leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
		return IronTier2Leggings;
	}
	
	public static ItemStack Tier2Boots(){
		ItemStack IronTier2Boots = new ItemStack(Material.IRON_BOOTS);
		IronTier2Boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
		return IronTier2Boots;
	}
	
	public static ItemStack Tier2Sword(){
		ItemStack IronTier2Sword = new ItemStack(Material.WOOD_SWORD);
		IronTier2Sword.addEnchantment(Enchantment.DAMAGE_ALL, 1);
		return IronTier2Sword;
	}
	
	//
	//Tier Three
	//
	public static ItemStack Tier3Helmet(){
		ItemStack IronTier3Helmet = new ItemStack(Material.IRON_HELMET);
		IronTier3Helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		return IronTier3Helmet;
	}
	
	public static ItemStack Tier3Chest(){
		ItemStack IronTier3Chest = new ItemStack(Material.IRON_CHESTPLATE);
		IronTier3Chest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		return IronTier3Chest;
	}
	
	public static ItemStack Tier3Leggings(){
		ItemStack IronTier3Leggings = new ItemStack(Material.IRON_LEGGINGS);
		IronTier3Leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		return IronTier3Leggings;
	}
	
	public static ItemStack Tier3Boots(){
		ItemStack IronTier3Boots = new ItemStack(Material.IRON_BOOTS);
		IronTier3Boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		return IronTier3Boots;
	}
	
	public static ItemStack Tier3Sword(){
		ItemStack IronTier3Sword = new ItemStack(Material.WOOD_SWORD);
		IronTier3Sword.addEnchantment(Enchantment.DAMAGE_ALL, 2);
		return IronTier3Sword;
	}
	
	//
	//Tier Four
	//
	public static ItemStack Tier4Helmet(){
		ItemStack IronTier4Helmet = new ItemStack(Material.IRON_HELMET);
		IronTier4Helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		IronTier4Helmet.setAmount(1);
		return IronTier4Helmet;
	}
	
	public static ItemStack Tier4Chest(){
		ItemStack IronTier4Chest = new ItemStack(Material.IRON_CHESTPLATE);
		IronTier4Chest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		IronTier4Chest.setAmount(1);
		return IronTier4Chest;
	}
	
	public static ItemStack Tier4Leggings(){
		ItemStack IronTier4Leggings = new ItemStack(Material.IRON_LEGGINGS);
		IronTier4Leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		IronTier4Leggings.setAmount(1);
		return IronTier4Leggings;
	}
	
	public static ItemStack Tier4Boots(){
		ItemStack IronTier4Boots = new ItemStack(Material.IRON_BOOTS);
		IronTier4Boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		IronTier4Boots.setAmount(1);
		return IronTier4Boots;
	}
	
	public static ItemStack Tier4Sword(){
		ItemStack IronTier4Sword = new ItemStack(Material.STONE_SWORD);
		IronTier4Sword.addEnchantment(Enchantment.DAMAGE_ALL, 3);
		IronTier4Sword.setAmount(1);
		return IronTier4Sword;
	}
	
}
