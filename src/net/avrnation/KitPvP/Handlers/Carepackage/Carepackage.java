package net.avrnation.KitPvP.Handlers.Carepackage;

import java.util.Random;

import net.avrnation.KitPvP.Main;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

public class Carepackage {

	static Random chesttype = new Random();
	static int chesttypechance;
	
	public static void Randommizer(){
		chesttypechance = chesttype.nextInt(2);
	}
	
	public static void SpawnCarepackage(Player player){
		Bukkit.broadcastMessage(ChatColor.GOLD + "Carepackage incoming for " + player.getName() + "!");
		Location location = player.getLocation();
		if(location.add(0, 1, 0).getBlock().getType() == Material.AIR){
			final Block block = location.getBlock();
			
			block.setType(Material.CHEST);
			
			Chest chest = (Chest) block.getState();
			Inventory inv = chest.getInventory();
			Randommizer();
			if(chesttypechance == 0){
				Potion speedpotion = new Potion(PotionType.SPEED, 1, false, true);
				ItemStack speedpotionstack = speedpotion.toItemStack(1);
				inv.addItem(new ItemStack(Material.ENDER_PEARL, 5));
				inv.addItem(speedpotionstack);
			} else if(chesttypechance == 1){
				inv.addItem(new ItemStack(Material.ENDER_PEARL, 5));
				inv.addItem(new ItemStack(Material.GOLDEN_APPLE, 5));
			} else if(chesttypechance == 2){
				Potion speedpotion = new Potion(PotionType.SPEED, 2, false, true);
				ItemStack speedpotionstack = speedpotion.toItemStack(1);
				inv.addItem(new ItemStack(Material.GOLDEN_APPLE, 5));
				inv.addItem(speedpotionstack);
			} else {
				Bukkit.broadcastMessage(ChatColor.RED + "Error: When creating ChestType for Carepackage!");
			}
			Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
				public void run() {
					block.setType(Material.AIR);
				}
			}, 200L);
		}
	}
}
