package net.avrnation.KitPvP.Handlers.Player;

import net.avrnation.KitPvP.Exceptions;
import net.avrnation.KitPvP.Global;
import net.avrnation.KitPvP.Managers.PurchaseUnlockable;
import net.avrnation.KitPvP.Object.HexaPlayer;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class PlayerChat implements Listener {

	@EventHandler
	public static void onPlayerChat(AsyncPlayerChatEvent event) throws Exceptions{
		HexaPlayer hexa = Global.getHexaPlayer(event.getPlayer());
		String unlockable;
		String message;
		if (hexa.getInteractiveMode()) {
			event.setCancelled(true);
			message = event.getMessage();
			unlockable = hexa.getInteractiveResponse();
			PurchaseUnlockable.Respond(message, hexa, unlockable);
		}
	}
}
