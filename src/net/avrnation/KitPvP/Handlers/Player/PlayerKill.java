package net.avrnation.KitPvP.Handlers.Player;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import net.avrnation.KitPvP.Global;
import net.avrnation.KitPvP.Handlers.Carepackage.Carepackage;
import net.avrnation.KitPvP.Object.HexaPlayer;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class PlayerKill implements Listener {

	public static Map<Player, Integer> killstreak = new ConcurrentHashMap<Player, Integer>();
	public static Map<Player, Integer> carepackagekills = new ConcurrentHashMap<Player, Integer>();
	
	@EventHandler
	public static void OnPlayerKill(PlayerDeathEvent event){
		Player killed = event.getEntity();
		Player killer = event.getEntity().getKiller().getPlayer();
		
		killed.sendMessage(ChatColor.AQUA + "You were just killed by " + killer.getName() + "!");
		killer.sendMessage(ChatColor.AQUA + "You have just killed " + killed.getName() + "!");
		
		//MySQLFunctions.GivePoints(killer);
		HexaPlayer hexakiller = Global.getHexaPlayer(killer);
		hexakiller.setKills(hexakiller.getKills() + 1);
		hexakiller.setPoints(hexakiller.getPoints() + 3);
		HexaPlayer hexakilled = Global.getHexaPlayer(killed);
		hexakilled.setDeaths(hexakilled.getDeaths() + 1);
		//Reset the Player Killeds, Kill Streak, and Carepackage
		killstreak.put(killed, 0);
		carepackagekills.put(killed, 0);
		
		//Update the Killers, Kill Streak, and Carepackage
		killstreak.put(killer, killstreak.get(killer) + 1); //Add 1 kill to Killers KillStreak
		carepackagekills.put(killer, carepackagekills.get(killer) + 1); //Add 1 kill to Killers Carepackage
		
		//Check if the Killers Carepackage Kills are divisible by 5, if so give them a Carepackage!
		if(carepackagekills.get(killer) % 5 == 0){
			Carepackage.SpawnCarepackage(killer);
		}
		//Check if the Killers Killstreak is divisible by 10, if so announce their killstreak!
		if(killstreak.get(killer) % 10 == 0){
			Bukkit.broadcastMessage(ChatColor.GOLD + "Wow! " + killer.getName() + " is on a " + killstreak.get(killer) + " kill KillStreak!");
		}
	}
}
