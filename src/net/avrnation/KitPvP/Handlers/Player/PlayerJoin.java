package net.avrnation.KitPvP.Handlers.Player;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import net.avrnation.KitPvP.Exceptions;
import net.avrnation.KitPvP.Global;
import net.avrnation.KitPvP.Log;
import net.avrnation.KitPvP.Handlers.MySQL.MySQL;
import net.avrnation.KitPvP.Object.HexaPlayer;
import net.avrnation.KitPvP.Utils.C;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

public class PlayerJoin implements Listener {

	private static ChatColor G = ChatColor.GREEN;
	private static ChatColor A = ChatColor.AQUA;
	private static ChatColor B = ChatColor.BOLD;
	private static String GB = (G + "" + B + "");
	private static String AB = (A + "" + B + "");
	
	@EventHandler
	public static void OnPlayerJoin(PlayerJoinEvent event){
		final Player player = event.getPlayer();
		
		/*Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
			public void run() {
				if(!(MySQLFunctions.ContainsPlayerUUID(player))){
					MySQLFunctions.InsertPlayer(player);
					player.sendMessage(C.GoldBold + "You have be successfully inserted into the Database!");
				}
				MessageOfTheDay(player);
			}
		}, 5L);*/
		
		PlayerJoinTask(player);
		Log.info("Player Login Event returned with " + player.getName());
		PlayerKill.killstreak.put(player, 0);
		PlayerKill.carepackagekills.put(player, 0);
	}
	
	public static void PlayerJoinTask(Player player){
		Log.info("Player Join Task returned with " + player.getName());
		HexaPlayer hexaplayer = null;
		try {
			hexaplayer = Global.getHexaPlayer(player.getName());
		} catch (Exceptions e1) {
			Log.info("Error getting the HexaPlayer!");
			e1.printStackTrace();
		}
		//Log.info("Got hexa player: " + hexaplayer.getName() + " From Player: " + player.getName());
		
		if(hexaplayer == null){
			hexaplayer = new HexaPlayer(player.getName());
			Log.info(player.getName() + "'s Hexa Profile was not found! Creating Hexa Profile for " + hexaplayer.getName() + "!");
			Global.addHexaPlayer(hexaplayer);
			Log.info("Added HexaProfile for " + hexaplayer.getName());
			String playername = player.getName();
			hexaplayer.setJoined(System.currentTimeMillis());
			hexaplayer.setLastOnline(System.currentTimeMillis());
			hexaplayer.setIP("");//Bukkit.getPlayer(hexaplayer.getUUID()).getAddress().toString());
			
			try {
				MySQL.openConnection();
				PreparedStatement newPlayer;
				newPlayer = MySQL.connection.prepareStatement("INSERT INTO `Player_Data` (uuid,currentname,lastOnline,joined,last_ip,points,kills,deaths,unlocked_kits) values(?,?,?,?,?,20,0,0,?);");
				newPlayer.setString(1, "broken");
				newPlayer.setString(2, hexaplayer.getName());
				newPlayer.setLong(3, hexaplayer.getLastOnline());
				newPlayer.setLong(4, hexaplayer.getJoined());
				newPlayer.setString(5, hexaplayer.getIP());
				newPlayer.setString(6, "none");
				newPlayer.execute();
				newPlayer.close();
				MySQL.closeConnection();
				Log.info("Succefully created Database entry for " + playername);
				player.sendMessage(C.GoldBold + "According to our Databases you are brand new to this server! Heres 20 Points to get you started! Have fun!");
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				MySQL.closeConnection();
			}
		} else {
			Log.info("Player was found! Not creating a HexaPlayer Profile! Profile returned as " + hexaplayer.getName() + " / " + player.getName());
		}
		hexaplayer.SaveNow();
		
		Global.updatePlayersOnline();
	}
	
	public static void ClearChat(final Player player){
		for(int i = 0; i < 20; i++){
			player.sendMessage("");
		}
	}
	
	public static void MessageOfTheDay(Player player){
		ClearChat(player);
		player.sendMessage(AB + "Welcome to Hexa PvP!");
		player.sendMessage(AB + "There are currently " + GB + Bukkit.getOnlinePlayers().length + AB + " Players Online!");
		player.sendMessage(AB + "Be sure to check out our Website! www.Hexa-Network.net");
	}
}
