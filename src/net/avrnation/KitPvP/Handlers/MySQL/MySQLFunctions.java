package net.avrnation.KitPvP.Handlers.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import net.avrnation.KitPvP.Utils.C;
import net.avrnation.KitPvP.Utils.MessageManager;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class MySQLFunctions {

	
	public static boolean ContainsPlayerName(Player player){
		String uuid = player.getName().toLowerCase();
		try {
			MySQL.openConnection();
			PreparedStatement sql = MySQL.connection.prepareStatement("SELECT * FROM `Player_Data` WHERE currentname=?;");
			sql.setString(1, uuid);
			ResultSet resultset = sql.executeQuery();
			boolean containsPlayer = resultset.next();
			sql.close();
			return containsPlayer;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MySQL.closeConnection();
		}
		return false;
	}
	
	/*public static boolean ContainsPlayerUUID(Player player){
		String uuid = player.getUniqueId().toString();
		try {
			MySQL.openConnection();
			PreparedStatement sql = MySQL.connection.prepareStatement("SELECT * FROM `Player_Data` WHERE uuid=?;");
			sql.setString(1, uuid);
			ResultSet resultset = sql.executeQuery();
			boolean containsPlayer = resultset.next();
			sql.close();
			return containsPlayer;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MySQL.closeConnection();
		}
		return false;
	}*/
		
	public static void GetPoints(Player player, Player sender){
		String uuid = player.getUniqueId().toString();
		int points = 0;
		try {
			MySQL.openConnection();
			PreparedStatement sql = MySQL.connection.prepareStatement("SELECT points FROM `Player_Data` WHERE uuid=?;");
			sql.setString(1, uuid);
			ResultSet result = sql.executeQuery();
			result.next();
			points = result.getInt("points");
			sql.close();
			result.close();
			sender.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + player.getName() + " has " + points + " Points!");
		} catch (Exception e) {
			e.printStackTrace();
			sender.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + player.getName() + " not found!");
		} finally {
			MySQL.closeConnection();
		}
	}
	
	public static int GetPoints(Player player){
		String uuid = player.getUniqueId().toString();
		int points = 0;
		try {
			MySQL.openConnection();
			PreparedStatement sql = MySQL.connection.prepareStatement("SELECT points FROM `Player_Data` WHERE uuid=?;");
			sql.setString(1, uuid);
			ResultSet result = sql.executeQuery();
			result.next();
			points = result.getInt("points");
			sql.close();
			result.close();
		} catch (Exception e) {
			e.printStackTrace();
			player.sendMessage(MessageManager.Error + "A horrible Error has occured. Please contact a Developer Immediatly, with the command typed!");
		} finally {
			MySQL.closeConnection();
		}
		return points;
	}
	
	public static int GetKills(Player player){
		String uuid = player.getUniqueId().toString();
		int kills = 0;
		try {
			MySQL.openConnection();
			PreparedStatement sql = MySQL.connection.prepareStatement("SELECT kills FROM `Player_Data` WHERE currentname=?;");
			//PreparedStatement sql = MySQL.connection.prepareStatement("SELECT kills FROM `Player_Data` WHERE uuid=?;");
			sql.setString(1, player.getName().toLowerCase());
			ResultSet result = sql.executeQuery();
			result.next();
			kills = result.getInt("kills");
			sql.close();
			result.close();
		} catch (Exception e) {
			e.printStackTrace();
			player.sendMessage(MessageManager.Error + "A horrible Error has occured. Please contact a Developer Immediatly, with the command typed!");
		} finally {
			MySQL.closeConnection();
		}
		return kills;
	}
	
	public static int GetDeaths(Player player){
		String uuid = player.getUniqueId().toString();
		int deaths = 0;
		try {
			MySQL.openConnection();
			PreparedStatement sql = MySQL.connection.prepareStatement("SELECT kills FROM `Player_Data` WHERE currentname=?;");
			//PreparedStatement sql = MySQL.connection.prepareStatement("SELECT kills FROM `Player_Data` WHERE uuid=?;");
			sql.setString(1, player.getName().toLowerCase());
			ResultSet result = sql.executeQuery();
			result.next();
			deaths = result.getInt("deaths");
			sql.close();
			result.close();
		} catch (Exception e) {
			e.printStackTrace();
			player.sendMessage(MessageManager.Error + "A horrible Error has occured. Please contact a Developer Immediatly, with the command typed!");
		} finally {
			MySQL.closeConnection();
		}
		return deaths;
	}
	
	public static void GivePoints(Player player, Player sender, String amount){
		String uuid = player.getUniqueId().toString();
		
		int additionalPoints = Integer.parseInt(amount);
		int oldPoints = 0;
		
		try {
			MySQL.openConnection();
			PreparedStatement sql = MySQL.connection.prepareStatement("SELECT kills FROM `Player_Data` WHERE currentname=?;");
			//PreparedStatement sql = MySQL.connection.prepareStatement("SELECT kills FROM `Player_Data` WHERE uuid=?;");
			sql.setString(1, player.getName().toLowerCase());
			ResultSet result = sql.executeQuery();
			result.next();
			oldPoints = result.getInt("points");
			sql.close();
			result.close();
			
			PreparedStatement pointsUpdate = MySQL.connection.prepareStatement("UPDATE `Player_Data` SET points=? WHERE currentname=?;");
			//PreparedStatement pointsUpdate = MySQL.connection.prepareStatement("UPDATE `Player_Data` SET points=? WHERE uuid=?;");
			pointsUpdate.setInt(1, oldPoints + additionalPoints);
			pointsUpdate.setString(2, player.getName().toLowerCase());
			pointsUpdate.executeUpdate();
			pointsUpdate.close();
			sql.close();
			result.close();
			
			sender.sendMessage(C.GoldBold + player.getName() + " now has " + (oldPoints + additionalPoints) + " Points!");
			if(player.isOnline()){
				player.sendMessage(C.AquaBold + sender.getName() + " has just given you " + additionalPoints + " Points!");
			} else {
				sender.sendMessage(C.GoldBold + player.getName() + " is not currently online. They will be notified when they log on!");
				//TODO Notification Of Point Change Code
			}
		} catch (Exception e) {
			e.printStackTrace();
			sender.sendMessage(MessageManager.Error + player.getName() + " not found!");
		} finally {
			MySQL.closeConnection();
		}
	}
	
	public static void TakePoints(Player player, Player sender, String amount){
		String uuid = player.getUniqueId().toString();
		
		int removalPoints = Integer.parseInt(amount);
		int oldPoints = 0;
		
		try {
			MySQL.openConnection();
			PreparedStatement sql = MySQL.connection.prepareStatement("SELECT points FROM `Player_Data` WHERE uuid=?;");
			sql.setString(1, uuid);
			ResultSet result = sql.executeQuery();
			result.next();
			oldPoints = result.getInt("points");
			sql.close();
			result.close();
			
			if(oldPoints - removalPoints >= 0){
				PreparedStatement pointsUpdate = MySQL.connection.prepareStatement("UPDATE `Player_Data` SET points=? WHERE uuid=?;");
				pointsUpdate.setInt(1, oldPoints - removalPoints);
				pointsUpdate.setString(2, uuid);
				pointsUpdate.executeUpdate();
				pointsUpdate.close();
				sql.close();
				result.close();
				
				sender.sendMessage(C.GoldBold + player.getName() + " now has " + (oldPoints + removalPoints) + " Points!");
			} else {
				sender.sendMessage(MessageManager.Error + player.getName() + " does not have " + amount + " Pointss in their Account!");
			}
			
			if(player.isOnline()){
				player.sendMessage(C.AquaBold + sender.getName() + " has just taken " + removalPoints + " Points from your Account!");
			} else {
				sender.sendMessage(C.GoldBold + player.getName() + " is not currently online. They will be notified when they log on!");
				//TODO Notification Of Points Change Code
			}
		} catch (Exception e) {
			e.printStackTrace();
			sender.sendMessage(MessageManager.Error + player.getName() + " not found!");
		} finally {
			MySQL.closeConnection();
		}
	}
	
	public static void InsertPlayer(Player player){
		String uuid = player.getUniqueId().toString();
		
		MySQL.openConnection();
		PreparedStatement newPlayer;
		try {
			newPlayer = MySQL.connection.prepareStatement("INSERT INTO `Player_Data` (uuid,currentname,points,kills,deaths) values(?,?,25,0,0);");
			newPlayer.setString(1, uuid);
			newPlayer.setString(2, player.getName());
			newPlayer.execute();
			newPlayer.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MySQL.closeConnection();
		}
	}
	
	public static void GivePoints(Player player){
		String uuid = player.getUniqueId().toString();
		int additionalPoints = 3;
		int oldPoints = 0;
		
		try {
			MySQL.openConnection();
			PreparedStatement sql = MySQL.connection.prepareStatement("SELECT points FROM `Player_Data` WHERE uuid=?;");
			sql.setString(1, uuid);
			ResultSet result = sql.executeQuery();
			result.next();
			oldPoints = result.getInt("points");
			sql.close();
			result.close();
			
			PreparedStatement pointsUpdate = MySQL.connection.prepareStatement("UPDATE `Player_Data` SET points=? WHERE uuid=?;");
			pointsUpdate.setInt(1, oldPoints + additionalPoints);
			pointsUpdate.setString(2, uuid);
			pointsUpdate.executeUpdate();
			pointsUpdate.close();
			sql.close();
			result.close();
			
			player.sendMessage(C.GoldBold + "You have been given" + C.GreenBold + "3" + C.GoldBold + "Points!");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MySQL.closeConnection();
		}
	}
	
	public static boolean CheckPoints(Player player, int amount){
		int removalPoints = amount;
		int oldPoints = 0;
		
		String uuid = player.getUniqueId().toString();
		
		try {
			MySQL.openConnection();
			PreparedStatement sql = MySQL.connection.prepareStatement("SELECT points FROM `Player_Data` WHERE uuid=?;");
			sql.setString(1, uuid);
			ResultSet result = sql.executeQuery();
			result.next();
			oldPoints = result.getInt("points");
			sql.close();
			result.close();
			
			if(oldPoints - removalPoints >= 0){
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			MySQL.closeConnection();
		}
	}
	
	public static void TakePoints(Player player, int amount, String packagename){
		int removalPoints = amount;
		int oldPoints = 0;
		
		String uuid = player.getUniqueId().toString();
		
		try {
			MySQL.openConnection();
			PreparedStatement sql = MySQL.connection.prepareStatement("SELECT points FROM `Player_Data` WHERE uuid=?;");
			sql.setString(1, uuid);
			ResultSet result = sql.executeQuery();
			result.next();
			oldPoints = result.getInt("points");
			sql.close();
			result.close();
			
			if(oldPoints - removalPoints >= 0){
				PreparedStatement pointsUpdate = MySQL.connection.prepareStatement("UPDATE `Player_Data` SET points=? WHERE uuid=?;");
				pointsUpdate.setInt(1, oldPoints - removalPoints);
				pointsUpdate.setString(2, uuid);
				pointsUpdate.executeUpdate();
				pointsUpdate.close();
				sql.close();
				result.close();
				
				player.sendMessage(C.GoldBold + "You just purchased the " + C.GreenBold + packagename + "!" + C.GoldBold + " Package!");
			} else {
				player.sendMessage(MessageManager.Error + "You do not have enough Points! " + "(" + amount + ")" + " Should never get this message...");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MySQL.closeConnection();
		}
	}
}