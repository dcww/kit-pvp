package net.avrnation.KitPvP.Handlers.Scoreboard;

import net.avrnation.KitPvP.Handlers.MySQL.MySQLFunctions;
import net.avrnation.KitPvP.Handlers.Player.PlayerKill;
import net.avrnation.KitPvP.Managers.SimpleScoreboard;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class ScoreboardTimer implements Runnable {

	@Override
	public void run() {
		for(Player player : Bukkit.getOnlinePlayers()){
			SimpleScoreboard scoreboard = new SimpleScoreboard("�c�lHexa-Network");
			scoreboard.add("�6�lWelcome -");
	        scoreboard.add("�b�l" + player.getName());
	        scoreboard.blankLine();
	        scoreboard.add("�6�lKills:");
	        scoreboard.add("�b�l" + MySQLFunctions.GetKills(player));
	        scoreboard.blankLine();
	        scoreboard.add("�6�lDeaths:");
	        scoreboard.add("�b�l" + MySQLFunctions.GetDeaths(player));
	        scoreboard.blankLine();
	        scoreboard.add("�6�lK/D:");
	        if(MySQLFunctions.GetDeaths(player) != 0){
	        	scoreboard.add("�b�l" + MySQLFunctions.GetKills(player) / MySQLFunctions.GetDeaths(player));
	        } else {
	        	scoreboard.add("�b�l0");
	        }
	        scoreboard.blankLine();
	        scoreboard.add("�6�lPoints:");
	        scoreboard.add("�b�l" + MySQLFunctions.GetPoints(player));
	        scoreboard.blankLine();
	        scoreboard.add("�6�lKillstreak:");
	        scoreboard.add("�b�l" + PlayerKill.killstreak.get(player));
	        scoreboard.blankLine();
	        scoreboard.add("�6�lOnline Players:");
	        scoreboard.add("�b�l" + Bukkit.getOnlinePlayers().length);
	        scoreboard.build();
	        scoreboard.send(player);
		}
	}
}

