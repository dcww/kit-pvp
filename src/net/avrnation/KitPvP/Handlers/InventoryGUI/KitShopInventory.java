package net.avrnation.KitPvP.Handlers.InventoryGUI;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class KitShopInventory {

public static void OpenGUI(Player player){
		
		Inventory KitPvPShop = Bukkit.createInventory(null, 36, ChatColor.AQUA + "Kit PvP Main");
		
		//Leather Armor
		ItemStack LeatherTier1 = new ItemStack(Material.LEATHER_CHESTPLATE);
		ItemMeta LeatherTier1Meta = LeatherTier1.getItemMeta();
		
		ItemStack LeatherTier2 = new ItemStack(Material.LEATHER_CHESTPLATE);
		ItemMeta LeatherTier2Meta = LeatherTier2.getItemMeta();
		
		ItemStack LeatherTier3 = new ItemStack(Material.LEATHER_CHESTPLATE);
		ItemMeta LeatherTier3Meta = LeatherTier3.getItemMeta();
		
		ItemStack LeatherTier4 = new ItemStack(Material.LEATHER_CHESTPLATE);
		ItemMeta LeatherTier4Meta = LeatherTier4.getItemMeta();
		
		//Chain Armor
		ItemStack ChainTier1 = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
		ItemMeta ChainTier1Meta = ChainTier1.getItemMeta();
		
		ItemStack ChainTier2 = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
		ItemMeta ChainTier2Meta = ChainTier2.getItemMeta();
		
		ItemStack ChainTier3 = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
		ItemMeta ChainTier3Meta = ChainTier3.getItemMeta();
		
		ItemStack ChainTier4 = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
		ItemMeta ChainTier4Meta = ChainTier4.getItemMeta();
		
		//Iron Armor
		ItemStack IronTier1 = new ItemStack(Material.IRON_CHESTPLATE);
		ItemMeta IronTier1Meta = IronTier1.getItemMeta();
		
		ItemStack IronTier2 = new ItemStack(Material.IRON_CHESTPLATE);
		ItemMeta IronTier2Meta = IronTier2.getItemMeta();
		
		ItemStack IronTier3 = new ItemStack(Material.IRON_CHESTPLATE);
		ItemMeta IronTier3Meta = IronTier3.getItemMeta();
		
		ItemStack IronTier4 = new ItemStack(Material.IRON_CHESTPLATE);
		ItemMeta IronTier4Meta = IronTier4.getItemMeta();
		
		//Diamond Armor
		ItemStack DiamondTier1 = new ItemStack(Material.DIAMOND_CHESTPLATE);
		ItemMeta DiamondTier1Meta = DiamondTier1.getItemMeta();
		
		ItemStack DiamondTier2 = new ItemStack(Material.DIAMOND_CHESTPLATE);
		ItemMeta DiamondTier2Meta = DiamondTier2.getItemMeta();
		
		ItemStack DiamondTier3 = new ItemStack(Material.DIAMOND_CHESTPLATE);
		ItemMeta DiamondTier3Meta = DiamondTier3.getItemMeta();
		
		ItemStack DiamondTier4 = new ItemStack(Material.DIAMOND_CHESTPLATE);
		ItemMeta DiamondTier4Meta = DiamondTier4.getItemMeta();
		
		LeatherTier1Meta.setDisplayName(ChatColor.AQUA + "Leather Tier 1");
		LeatherTier1Meta.setLore(Arrays.asList(ChatColor.GOLD + "" + ChatColor.BOLD + "Kit: " + ChatColor.AQUA + "Leather",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Tier: " + ChatColor.AQUA + "1",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Cost: " + ChatColor.AQUA + "9 Points = 3 Kills",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Unlock Cost: " + ChatColor.AQUA + "3 Points",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Included: ",
				ChatColor.AQUA + "* Wood Sword",
				ChatColor.AQUA + "* Leather Armor | Protection 1"
				));
		LeatherTier1.setItemMeta(LeatherTier1Meta);
		
		LeatherTier2Meta.setDisplayName(ChatColor.AQUA + "Leather Tier 2");
		LeatherTier2Meta.setLore(Arrays.asList(ChatColor.GOLD + "" + ChatColor.BOLD + "Kit: " + ChatColor.AQUA + "Leather",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Tier: " + ChatColor.AQUA + "2",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Cost: " + ChatColor.AQUA + "15 Points = 5 Kills",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Unlock Cost: " + ChatColor.AQUA + "6 Points = 2 Kills",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Included: ",
				ChatColor.AQUA + "* Wood Sword | Sharpness 1",
				ChatColor.AQUA + "* Leather Armor | Protection 2"
				));
		LeatherTier2.setItemMeta(LeatherTier2Meta);
		
		LeatherTier3Meta.setDisplayName(ChatColor.AQUA + "Leather Tier 3");
		LeatherTier3Meta.setLore(Arrays.asList(ChatColor.GOLD + "" + ChatColor.BOLD + "Kit: " + ChatColor.AQUA + "Leather",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Tier: " + ChatColor.AQUA + "3",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Cost: " + ChatColor.AQUA + "21 Points = 7 Kills",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Unlock Cost: " + ChatColor.AQUA + "9 Points = 3 Kills",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Included: ",
				ChatColor.AQUA + "* Wood Sword | Sharpness 2",
				ChatColor.AQUA + "* Leather Armor | Protection 3"
				));
		LeatherTier3.setItemMeta(LeatherTier3Meta);
		
		LeatherTier4Meta.setDisplayName(ChatColor.AQUA + "Leather Tier 4");
		LeatherTier4Meta.setLore(Arrays.asList(ChatColor.GOLD + "" + ChatColor.BOLD + "Kit: " + ChatColor.AQUA + "Leather",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Tier: " + ChatColor.AQUA + "4",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Cost: " + ChatColor.AQUA + "27 Points = 9 Kills",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Unlock Cost: " + ChatColor.AQUA + "12 Points = 4 Kills",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Included: ",
				ChatColor.AQUA + "* Wood Sword | Sharpness 3",
				ChatColor.AQUA + "* Leather Armor | Protection 4"
				));
		LeatherTier4.setItemMeta(LeatherTier4Meta);
		
		//
		//Chain Armor
		//
		ChainTier1Meta.setDisplayName(ChatColor.AQUA + "Chain Tier 1");
		ChainTier1Meta.setLore(Arrays.asList(ChatColor.GOLD + "" + ChatColor.BOLD + "Kit: " + ChatColor.AQUA + "Chain",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Tier: " + ChatColor.AQUA + "1",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Cost: " + ChatColor.AQUA + "30 Points = 10 Kills",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Unlock Cost: " + ChatColor.AQUA + "9 Points",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Included: ",
				ChatColor.AQUA + "* Chain Sword",
				ChatColor.AQUA + "* Chain Armor | Protection 1"
				));
		ChainTier1.setItemMeta(ChainTier1Meta);
		
		ChainTier2Meta.setDisplayName(ChatColor.AQUA + "Chain Tier 2");
		ChainTier2Meta.setLore(Arrays.asList(ChatColor.GOLD + "" + ChatColor.BOLD + "Kit: " + ChatColor.AQUA + "Chain",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Tier: " + ChatColor.AQUA + "2",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Cost: " + ChatColor.AQUA + "36 Points = 12 Kills",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Unlock Cost: " + ChatColor.AQUA + "15 Points",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Included: ",
				ChatColor.AQUA + "* Chain Sword | Sharpness 1",
				ChatColor.AQUA + "* Chain Armor | Protection 2"
				));
		ChainTier2.setItemMeta(ChainTier2Meta);
		
		ChainTier3Meta.setDisplayName(ChatColor.AQUA + "Chain Tier 3");
		ChainTier3Meta.setLore(Arrays.asList(ChatColor.GOLD + "" + ChatColor.BOLD + "Kit: " + ChatColor.AQUA + "Chain",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Tier: " + ChatColor.AQUA + "3",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Cost: " + ChatColor.AQUA + "42 Points = 14 Kills",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Unlock Cost: " + ChatColor.AQUA + "21 Points",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Included: ",
				ChatColor.AQUA + "* Chain Sword | Sharpness 2",
				ChatColor.AQUA + "* Chain Armor | Protection 3"
				));
		ChainTier3.setItemMeta(ChainTier3Meta);
		
		ChainTier4Meta.setDisplayName(ChatColor.AQUA + "Chain Tier 4");
		ChainTier4Meta.setLore(Arrays.asList(ChatColor.GOLD + "" + ChatColor.BOLD + "Kit: " + ChatColor.AQUA + "Chain",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Tier: " + ChatColor.AQUA + "4",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Cost: " + ChatColor.AQUA + "48 Points = 16 Kills",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Unlock Cost: " + ChatColor.AQUA + "27 Points",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Included: ",
				ChatColor.AQUA + "* Chain Sword | Sharpness 3",
				ChatColor.AQUA + "* Chain Armor | Protection 4"
				));
		ChainTier4.setItemMeta(ChainTier4Meta);
		
		//
		//Iron Armor
		//
		IronTier1Meta.setDisplayName(ChatColor.AQUA + "Iron Tier 1");
		IronTier1Meta.setLore(Arrays.asList(ChatColor.GOLD + "" + ChatColor.BOLD + "Kit: " + ChatColor.AQUA + "Iron",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Tier: " + ChatColor.AQUA + "1",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Cost: " + ChatColor.AQUA + "51 Points = 17 Kills",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Unlock Cost: " + ChatColor.AQUA + "33 Points",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Included: ",
				ChatColor.AQUA + "* Iron Sword",
				ChatColor.AQUA + "* Iron Armor | Protection 1"
				));
		IronTier1.setItemMeta(IronTier1Meta);
		
		IronTier2Meta.setDisplayName(ChatColor.AQUA + "Iron Tier 2");
		IronTier2Meta.setLore(Arrays.asList(ChatColor.GOLD + "" + ChatColor.BOLD + "Kit: " + ChatColor.AQUA + "Iron",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Tier: " + ChatColor.AQUA + "2",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Cost: " + ChatColor.AQUA + "57 Points = 19 Kills",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Unlock Cost: " + ChatColor.AQUA + "39 Points",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Included: ",
				ChatColor.AQUA + "* Iron Sword | Sharpness 1",
				ChatColor.AQUA + "* Iron Armor | Protection 2"
				));
		IronTier2.setItemMeta(IronTier2Meta);
		
		IronTier3Meta.setDisplayName(ChatColor.AQUA + "Iron Tier 3");
		IronTier3Meta.setLore(Arrays.asList(ChatColor.GOLD + "" + ChatColor.BOLD + "Kit: " + ChatColor.AQUA + "Iron",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Tier: " + ChatColor.AQUA + "3",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Cost: " + ChatColor.AQUA + "63 Points = 21 Kills",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Unlock Cost: " + ChatColor.AQUA + "45 Points",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Included: ",
				ChatColor.AQUA + "* Iron Sword | Sharpness 2",
				ChatColor.AQUA + "* Iron Armor | Protection 3"
				));
		IronTier3.setItemMeta(IronTier3Meta);
		
		IronTier4Meta.setDisplayName(ChatColor.AQUA + "Iron Tier 4");
		IronTier4Meta.setLore(Arrays.asList(ChatColor.GOLD + "" + ChatColor.BOLD + "Kit: " + ChatColor.AQUA + "Iron",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Tier: " + ChatColor.AQUA + "4",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Cost: " + ChatColor.AQUA + "69 Points = 23 Kills",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Unlock Cost: " + ChatColor.AQUA + "51 Points",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Included: ",
				ChatColor.AQUA + "* Iron Sword | Sharpness 3",
				ChatColor.AQUA + "* Iron Armor | Protection 4"
				));
		IronTier4.setItemMeta(IronTier4Meta);
		
		//
		//Diamond Armor
		//
		DiamondTier1Meta.setDisplayName(ChatColor.AQUA + "Diamond Tier 1");
		DiamondTier1Meta.setLore(Arrays.asList(ChatColor.GOLD + "" + ChatColor.BOLD + "Kit: " + ChatColor.AQUA + "Diamond",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Tier: " + ChatColor.AQUA + "1",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Cost: " + ChatColor.AQUA + "72 Points = 24 Kills",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Unlock Cost: " + ChatColor.AQUA + "57 Points",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Included: ",
				ChatColor.AQUA + "* Diamond Sword",
				ChatColor.AQUA + "* Diamond Armor | Protection 1"
				));
		DiamondTier1.setItemMeta(DiamondTier1Meta);
		
		DiamondTier2Meta.setDisplayName(ChatColor.AQUA + "Diamond Tier 2");
		DiamondTier2Meta.setLore(Arrays.asList(ChatColor.GOLD + "" + ChatColor.BOLD + "Kit: " + ChatColor.AQUA + "Diamond",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Tier: " + ChatColor.AQUA + "2",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Cost: " + ChatColor.AQUA + "78 Points = 26 Kills",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Unlock Cost: " + ChatColor.AQUA + "63 Points",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Included: ",
				ChatColor.AQUA + "* Diamond Sword | Sharpness 1",
				ChatColor.AQUA + "* Diamond Armor | Protection 2"
				));
		DiamondTier2.setItemMeta(DiamondTier2Meta);
		
		DiamondTier3Meta.setDisplayName(ChatColor.AQUA + "Diamond Tier 3");
		DiamondTier3Meta.setLore(Arrays.asList(ChatColor.GOLD + "" + ChatColor.BOLD + "Kit: " + ChatColor.AQUA + "Diamond",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Tier: " + ChatColor.AQUA + "3",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Cost: " + ChatColor.AQUA + "84 Points = 28 Kills",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Unlock Cost: " + ChatColor.AQUA + "69 Points",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Included: ",
				ChatColor.AQUA + "* Diamond Sword | Sharpness 2",
				ChatColor.AQUA + "* Diamond Armor | Protection 3"
				));
		DiamondTier3.setItemMeta(DiamondTier3Meta);
		
		DiamondTier4Meta.setDisplayName(ChatColor.AQUA + "Diamond Tier 4");
		DiamondTier4Meta.setLore(Arrays.asList(ChatColor.GOLD + "" + ChatColor.BOLD + "Kit: " + ChatColor.AQUA + "Diamond",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Tier: " + ChatColor.AQUA + "4",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Cost: " + ChatColor.AQUA + "90 Points = 30 Kills",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Unlock Cost: " + ChatColor.AQUA + "75 Points",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Included: ",
				ChatColor.AQUA + "* Diamond Sword | Sharpness 3",
				ChatColor.AQUA + "* Diamond Armor | Protection 4"
				));
		DiamondTier4.setItemMeta(DiamondTier4Meta);
		
		KitPvPShop.setItem(0, LeatherTier1);
		KitPvPShop.setItem(1, LeatherTier2);
		KitPvPShop.setItem(2, LeatherTier3);
		KitPvPShop.setItem(3, LeatherTier4);
		
		KitPvPShop.setItem(9, ChainTier1);
		KitPvPShop.setItem(10, ChainTier2);
		KitPvPShop.setItem(11, ChainTier3);
		KitPvPShop.setItem(12, ChainTier4);
		
		KitPvPShop.setItem(18, IronTier1);
		KitPvPShop.setItem(19, IronTier2);
		KitPvPShop.setItem(20, IronTier3);
		KitPvPShop.setItem(21, IronTier4);
		
		KitPvPShop.setItem(27, DiamondTier1);
		KitPvPShop.setItem(28, DiamondTier2);
		KitPvPShop.setItem(29, DiamondTier3);
		KitPvPShop.setItem(30, DiamondTier4);
		
		player.openInventory(KitPvPShop);
	}
}
