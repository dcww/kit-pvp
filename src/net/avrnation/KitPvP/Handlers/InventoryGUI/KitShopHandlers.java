package net.avrnation.KitPvP.Handlers.InventoryGUI;

import net.avrnation.KitPvP.Global;
import net.avrnation.KitPvP.Handlers.Kits.KitManager;
import net.avrnation.KitPvP.Handlers.Kits.KitsFull.ChainKits;
import net.avrnation.KitPvP.Handlers.Kits.KitsFull.DiamondKits;
import net.avrnation.KitPvP.Handlers.Kits.KitsFull.IronKits;
import net.avrnation.KitPvP.Handlers.Kits.KitsFull.LeatherKits;
import net.avrnation.KitPvP.Managers.Confirmination;
import net.avrnation.KitPvP.Object.HexaPlayer;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class KitShopHandlers implements Listener {
	
	@EventHandler
	public static void onPlayerClickEvent(InventoryClickEvent event){
		final Player player = (Player) event.getWhoClicked();
		HexaPlayer hexa = Global.getHexaPlayer(player);
		
		if (event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR || !(event.getCurrentItem().hasItemMeta())) {
			return;
		}
		
		ItemStack item = event.getCurrentItem();
		if(item.getItemMeta().getLore() != null){
			if(item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Leather Tier 1")){
				event.setCancelled(true);
				player.closeInventory();
				if (hexa.unlocked.contains("LTeir1Upgrade")) {
					if (hexa.getPoints() >= KitManager.LTeir1Cost) {
						if (CheckInventory(player)) {
							hexa.setPoints(hexa.getPoints() - KitManager.LTeir1Cost);
							hexa.SaveNow();
							LeatherKits.Teir1(player);
							player.updateInventory();
						}
					}
				} else {
					Confirmination.UnlockPurchase(hexa, "LTeir1Upgrade", KitManager.LTeir1Upgrade);
				}
			} else if(item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Leather Tier 2")){
				event.setCancelled(true);
				player.closeInventory();
				if (hexa.unlocked.contains("LTeir2Upgrade")) {
					if (hexa.getPoints() >= KitManager.LTeir2Cost) {
						if (CheckInventory(player)) {
							hexa.setPoints(hexa.getPoints() - KitManager.LTeir2Cost);
							hexa.SaveNow();
							LeatherKits.Teir2(player);
							player.updateInventory();
						}
					}
				} else {
					Confirmination.UnlockPurchase(hexa, "LTeir2Upgrade", KitManager.LTeir2Upgrade);
				}
			} else if(item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Leather Tier 3")){
				event.setCancelled(true);
				player.closeInventory();
				if (hexa.unlocked.contains("LTeir3Upgrade")) {
					if (hexa.getPoints() >= KitManager.LTeir3Cost) {
						if (CheckInventory(player)) {
							hexa.setPoints(hexa.getPoints() - KitManager.LTeir3Cost);
							hexa.SaveNow();
							LeatherKits.Teir3(player);
							player.updateInventory();
						}
					}
				} else {
					Confirmination.UnlockPurchase(hexa, "LTeir3Upgrade", KitManager.LTeir3Upgrade);
				}
			} else if(item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Leather Tier 4")){
				event.setCancelled(true);
				player.closeInventory();
				if (hexa.unlocked.contains("LTeir4Upgrade")) {
					if (hexa.getPoints() >= KitManager.LTeir4Cost) {
						if (CheckInventory(player)) {
							hexa.setPoints(hexa.getPoints() - KitManager.LTeir4Cost);
							hexa.SaveNow();
							LeatherKits.Teir4(player);
							player.updateInventory();
						}
					}
				} else {
					Confirmination.UnlockPurchase(hexa, "LTeir4Upgrade", KitManager.LTeir4Upgrade);
				}
			} else if(item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Chain Tier 1")){
				event.setCancelled(true);
				player.closeInventory();
				if (hexa.unlocked.contains("CTeir1Upgrade")) {
					if (hexa.getPoints() >= KitManager.CTeir1Cost) {
						if (CheckInventory(player)) {
							hexa.setPoints(hexa.getPoints() - KitManager.CTeir1Cost);
							hexa.SaveNow();
							ChainKits.Teir1(player);
							player.updateInventory();
						}
					}
				} else {
					Confirmination.UnlockPurchase(hexa, "CTeir1Upgrade", KitManager.CTeir1Upgrade);
				}
			} else if(item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Chain Tier 2")){
				event.setCancelled(true);
				player.closeInventory();
				if (hexa.unlocked.contains("CTeir2Upgrade")) {
					if (hexa.getPoints() >= KitManager.CTeir2Cost) {
						if (CheckInventory(player)) {
							hexa.setPoints(hexa.getPoints() - KitManager.CTeir2Cost);
							hexa.SaveNow();
							ChainKits.Teir2(player);
							player.updateInventory();
						}
					}
				} else {
					Confirmination.UnlockPurchase(hexa, "CTeir2Upgrade", KitManager.CTeir2Upgrade);
				}
			} else if(item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Chain Tier 3")){
				event.setCancelled(true);
				player.closeInventory();
				if (hexa.unlocked.contains("CTeir3Upgrade")) {
					if (hexa.getPoints() >= KitManager.CTeir3Cost) {
						if (CheckInventory(player)) {
							hexa.setPoints(hexa.getPoints() - KitManager.CTeir3Cost);
							hexa.SaveNow();
							ChainKits.Teir3(player);
							player.updateInventory();
						}
					}
				} else {
					Confirmination.UnlockPurchase(hexa, "CTeir3Upgrade", KitManager.CTeir3Upgrade);
				}
			} else if(item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Chain Tier 4")){
				event.setCancelled(true);
				player.closeInventory();
				if (hexa.unlocked.contains("CTeir4Upgrade")) {
					if (hexa.getPoints() >= KitManager.CTeir4Cost) {
						if (CheckInventory(player)) {
							hexa.setPoints(hexa.getPoints() - KitManager.CTeir4Cost);
							hexa.SaveNow();
							ChainKits.Teir4(player);
							player.updateInventory();
						}
					}
				} else {
					Confirmination.UnlockPurchase(hexa, "CTeir4Upgrade", KitManager.CTeir4Upgrade);
				}
			} else if(item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Iron Tier 1")){
				event.setCancelled(true);
				player.closeInventory();
				if (hexa.unlocked.contains("ITeir1Upgrade")) {
					if (hexa.getPoints() >= KitManager.ITeir1Cost) {
						if (CheckInventory(player)) {
							hexa.setPoints(hexa.getPoints() - KitManager.ITeir1Cost);
							hexa.SaveNow();
							IronKits.Teir1(player);
							player.updateInventory();
						}
					}
				} else {
					Confirmination.UnlockPurchase(hexa, "ITeir1Upgrade", KitManager.ITeir1Upgrade);
				}
			} else if(item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Iron Tier 2")){
				event.setCancelled(true);
				player.closeInventory();
				if (hexa.unlocked.contains("ITeir2Upgrade")) {
					if (hexa.getPoints() >= KitManager.ITeir2Cost) {
						if (CheckInventory(player)) {
							hexa.setPoints(hexa.getPoints() - KitManager.ITeir2Cost);
							hexa.SaveNow();
							IronKits.Teir2(player);
							player.updateInventory();
						}
					}
				} else {
					Confirmination.UnlockPurchase(hexa, "ITeir2Upgrade", KitManager.ITeir2Upgrade);
				}
			} else if(item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Iron Tier 3")){
				event.setCancelled(true);
				player.closeInventory();
				if (hexa.unlocked.contains("ITeir3Upgrade")) {
					if (hexa.getPoints() >= KitManager.ITeir3Cost) {
						if (CheckInventory(player)) {
							hexa.setPoints(hexa.getPoints() - KitManager.ITeir3Cost);
							hexa.SaveNow();
							IronKits.Teir3(player);
							player.updateInventory();
						}
					}
				} else {
					Confirmination.UnlockPurchase(hexa, "ITeir3Upgrade", KitManager.ITeir3Upgrade);
				}
			} else if(item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Iron Tier 4")){
				event.setCancelled(true);
				player.closeInventory();
				if (hexa.unlocked.contains("ITeir4Upgrade")) {
					if (hexa.getPoints() >= KitManager.ITeir4Cost) {
						if (CheckInventory(player)) {
							hexa.setPoints(hexa.getPoints() - KitManager.ITeir4Cost);
							hexa.SaveNow();
							IronKits.Teir4(player);
							player.updateInventory();
						}
					}
				} else {
					Confirmination.UnlockPurchase(hexa, "ITeir4Upgrade", KitManager.ITeir4Upgrade);
				}
			} else if(item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Diamond Tier 1")){
				event.setCancelled(true);
				player.closeInventory();
				if (hexa.unlocked.contains("DTeir1Upgrade")) {
					if (hexa.getPoints() >= KitManager.DTeir1Cost) {
						if (CheckInventory(player)) {
							hexa.setPoints(hexa.getPoints() - KitManager.DTeir1Cost);
							hexa.SaveNow();
							DiamondKits.Teir1(player);
							player.updateInventory();
						}
					}
				} else {
					Confirmination.UnlockPurchase(hexa, "DTeir1Upgrade", KitManager.DTeir1Upgrade);
				}
			} else if(item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Diamond Tier 2")){
				event.setCancelled(true);
				player.closeInventory();
				if (hexa.unlocked.contains("DTeir2Upgrade")) {
					if (hexa.getPoints() >= KitManager.DTeir2Cost) {
						if (CheckInventory(player)) {
							hexa.setPoints(hexa.getPoints() - KitManager.DTeir2Cost);
							hexa.SaveNow();
							DiamondKits.Teir2(player);
							player.updateInventory();
						}
					}
				} else {
					Confirmination.UnlockPurchase(hexa, "DTeir2Upgrade", KitManager.DTeir2Upgrade);
				}
			} else if(item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Diamond Tier 3")){
				event.setCancelled(true);
				player.closeInventory();
				if (hexa.unlocked.contains("DTeir3Upgrade")) {
					if (hexa.getPoints() >= KitManager.DTeir3Cost) {
						if (CheckInventory(player)) {
							hexa.setPoints(hexa.getPoints() - KitManager.DTeir3Cost);
							hexa.SaveNow();
							DiamondKits.Teir3(player);
							player.updateInventory();
						}
					}
				} else {
					Confirmination.UnlockPurchase(hexa, "DTeir3Upgrade", KitManager.DTeir3Upgrade);
				}
			} else if(item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Diamond Tier 4")){
				event.setCancelled(true);
				player.closeInventory();
				if (hexa.unlocked.contains("DTeir4Upgrade")) {
					if (hexa.getPoints() >= KitManager.DTeir4Cost) {
						if (CheckInventory(player)) {
							hexa.setPoints(hexa.getPoints() - KitManager.DTeir4Cost);
							hexa.SaveNow();
							DiamondKits.Teir4(player);
							player.updateInventory();
						}
					}
				} else {
					Confirmination.UnlockPurchase(hexa, "DTeir4Upgrade", KitManager.DTeir4Upgrade);
				}
			}
		}
	}
	
	public static boolean CheckInventory(Player player){
		int count = 0;
		for (int i = 0; i < 37; i++) {
			if(player.getInventory().getItem(i) == null){
				count++;
			}
		}
		if(count >= 5){
			return true;
		} else {
			return false;
		}
	}
}
