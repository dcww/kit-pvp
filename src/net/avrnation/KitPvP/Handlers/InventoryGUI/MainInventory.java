package net.avrnation.KitPvP.Handlers.InventoryGUI;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class MainInventory {
	
	public static void OpenGUI(Player player){
		
		Inventory KitPvPMain = Bukkit.createInventory(null, 9, ChatColor.AQUA + "Kit PvP Main");
		
		ItemStack FreeKit = new ItemStack(Material.LEATHER_CHESTPLATE);
		ItemMeta FreeKitMeta = FreeKit.getItemMeta();
		
		ItemStack DailyKit = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
		ItemMeta DailyKitMeta = DailyKit.getItemMeta();
		
		ItemStack WeeklyKit = new ItemStack(Material.IRON_CHESTPLATE);
		ItemMeta WeeklyKitMeta = WeeklyKit.getItemMeta();
		
		ItemStack Shop = new ItemStack(Material.DIAMOND_CHESTPLATE);
		ItemMeta ShopMeta = Shop.getItemMeta();

		DailyKit.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		WeeklyKit.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
		
		FreeKitMeta.setDisplayName(ChatColor.AQUA + "Free Kit");
		FreeKitMeta.setLore(Arrays.asList(ChatColor.GOLD + "" + ChatColor.BOLD + "Kit: " + ChatColor.AQUA + "Free",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Cost: " + ChatColor.AQUA + "0 Points | 0 Hours",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Included: ",
				ChatColor.AQUA + "* Wood Sword",
				ChatColor.AQUA + "* Leather Armor"
				));
		FreeKit.setItemMeta(FreeKitMeta);
		
		DailyKitMeta.setDisplayName(ChatColor.AQUA + "Daily Kit");
		DailyKitMeta.setLore(Arrays.asList(ChatColor.GOLD + "" + ChatColor.BOLD + "Kit: " + ChatColor.AQUA + "Daily",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Cost: " + ChatColor.AQUA + "0 Points | 24 Hours",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Included: ",
				ChatColor.AQUA + "* Stone Sword | Sharp 1",
				ChatColor.AQUA + "* Iron Armor | Prot 1",
				ChatColor.AQUA + "* Golden Apple | 4",
				ChatColor.AQUA + "* Ender Pearl | 6",
				ChatColor.AQUA + "* Speed Potion (8:00) | 1"
				));
		DailyKit.setItemMeta(DailyKitMeta);
		
		WeeklyKitMeta.setDisplayName(ChatColor.AQUA + "Weekly Kit");
		WeeklyKitMeta.setLore(Arrays.asList(ChatColor.GOLD + "" + ChatColor.BOLD + "Kit: " + ChatColor.AQUA + "Daily",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Cost: " + ChatColor.AQUA + "0 Points | 24 Hours",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Included: ",
				ChatColor.AQUA + "* Stone Sword | Sharp 2",
				ChatColor.AQUA + "* Iron Armor | Prot 2",
				ChatColor.AQUA + "* Golden Apple | 8",
				ChatColor.AQUA + "* Ender Pearl | 12",
				ChatColor.AQUA + "* Speed Potion (8:00) | 2",
				ChatColor.AQUA + "* Strength Potion II (1:30) | 1"
				));
		WeeklyKit.setItemMeta(WeeklyKitMeta);
		
		ShopMeta.setDisplayName(ChatColor.AQUA + "Shop");
		ShopMeta.setLore(Arrays.asList(ChatColor.DARK_GREEN + "The Kit Shop"));
		Shop.setItemMeta(ShopMeta);
		
		KitPvPMain.setItem(0, FreeKit);
		KitPvPMain.setItem(1, DailyKit);
		KitPvPMain.setItem(2, WeeklyKit);
		KitPvPMain.setItem(3, Shop);
		
		player.openInventory(KitPvPMain);
	}
}
