package net.avrnation.KitPvP.Handlers.InventoryGUI;

import net.avrnation.KitPvP.Main;
import net.avrnation.KitPvP.Handlers.Kits.KitsFull.IronKits;
import net.avrnation.KitPvP.Handlers.Kits.KitsFull.OtherKits;
import net.avrnation.KitPvP.Utils.C;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class InventoryGUIManager implements Listener {

	@EventHandler
	public static void onPlayerClickEvent(InventoryClickEvent event){
		final Player player = (Player) event.getWhoClicked();
		
		if (event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR || !(event.getCurrentItem().hasItemMeta())) {
			return;
		}
		
		ItemStack item = event.getCurrentItem();
		if(item.getItemMeta().getDisplayName() == null){
			return;
		}
		if(item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Free Kit")){
			event.setCancelled(true);
			player.closeInventory();
			OtherKits.Free(player);
		} else if(item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Daily Kit")){
			event.setCancelled(true);
			player.closeInventory();
			player.sendMessage(C.GoldBold + "Coming Soon!");
		} else if(item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Weekly Kit")){
			event.setCancelled(true);
			player.closeInventory();
			player.sendMessage(C.GoldBold + "Coming Soon!");
		} else if(item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Shop")){
			event.setCancelled(true);
			player.closeInventory();
			Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
				  public void run() {
					  KitShopInventory.OpenGUI(player);
				  }
			}, 2L);
		}
	}
}
