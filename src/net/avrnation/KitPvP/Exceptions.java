package net.avrnation.KitPvP;

public class Exceptions extends Exception {

	private static final long serialVersionUID = 2752707013644337603L;

	public Exceptions(String message) {
		super(message);
	}
}
