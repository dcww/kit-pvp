package net.avrnation.KitPvP;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import net.avrnation.KitPvP.Handlers.MySQL.MySQL;
import net.avrnation.KitPvP.Object.HexaPlayer;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Global {
	public static Map<String, HexaPlayer> hexaplayers = new ConcurrentHashMap<String, HexaPlayer>();
	public static Map<UUID, HexaPlayer> hexaplayersViaUUID = new ConcurrentHashMap<UUID, HexaPlayer>();
	
	public static ArrayList<Player> OnlinePlayers = new ArrayList<Player>();
	
	public static String playerName;
	
	public static HexaPlayer getHexaPlayer(Player player){
		return hexaplayers.get(player.getName().toLowerCase());
	}
	
	public static Player getPlayer(String name) throws Exceptions{
		Player player = Bukkit.getPlayer(name);
		if(player == null){
			throw new Exceptions("No Hexa Profile found for " + name + "!");
		}
		return player;
	}
	
	public static void loadHexaPlayers() throws SQLException {
		try {
			MySQL.openConnection();
			PreparedStatement sql = MySQL.connection.prepareStatement("SELECT * FROM `Player_Data`;");
			ResultSet result = sql.executeQuery();
			while(result.next()){
				HexaPlayer hexa;
				try {
					hexa = new HexaPlayer(result);
					hexaplayers.put(hexa.getName().toLowerCase(), hexa);
					//Global.addHexaPlayer(hexa);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			sql.close();
			result.close();
			Log.info("Loaded " + hexaplayers.size() + " HexaPlayers!");
		} catch (SQLException e) {
			Log.error("MySQL Exception when loading HexaPlayers");
			e.printStackTrace();
		} finally {
			MySQL.closeConnection();
		}
	}
	
	public static void updatePlayersOnline(){
		OnlinePlayers.clear();
		for(Player player : Bukkit.getOnlinePlayers()){
			OnlinePlayers.add(player);
		}
	}
	
	public static void addHexaPlayer(HexaPlayer hexa) {
 		hexaplayers.put(hexa.getName().toLowerCase(), hexa);
 		//hexaplayersViaUUID.put(hexa.getUUID(), hexa);
 	}
 	
 	public static void removeHexaPlayer(HexaPlayer hexa) {
 		hexaplayers.remove(hexa.getName().toLowerCase());
 		//hexaplayersViaUUID.remove(hexa.getUUID());
 	}
 
 	public static Player getPlayer(HexaPlayer hexa) throws Exceptions {
 		Player player = Bukkit.getPlayer(hexa.getName());
 		if(player == null){
 			throw new Exceptions("No HexaPlayer Profile found by the name of " + hexa.getName().toLowerCase() + "!");
 		}
 		return player;
 	}
 	
 	//public static HexaPlayer getHexaPlayerViaUUID(UUID uuid) {
 	//	return hexaplayersViaUUID.get(uuid);
 	//}
 	
 	public static HexaPlayer getHexaPlayer(String name) throws Exceptions {
 		//HexaPlayer hexa = Global.getHexaPlayer(name.toLowerCase());
		//Player player = Bukkit.getPlayer(hexa.getUUID());
 		//if (player == null) {
 		//	throw new Exceptions("No player named " + name);
 		//}
 		//if (hexaplayers.containsKey(name.toLowerCase())) {
 		//	return hexaplayers.get(name.toLowerCase());
 		//}
 		
 		return hexaplayers.get(name.toLowerCase());
 	}
}
