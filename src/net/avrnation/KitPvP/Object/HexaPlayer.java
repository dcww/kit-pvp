package net.avrnation.KitPvP.Object;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

import net.avrnation.KitPvP.Log;
import net.avrnation.KitPvP.Handlers.MySQL.MySQL;

public class HexaPlayer {

	public ArrayList<String> unlocked = new ArrayList<String>();
	
	private static String playername = null;
	
	private int id;
	private long joined;
	private long lastOnline;
	private static String lastIP = "";
	private String currentIP = "";
	private Boolean newIP = false;
	private static UUID uid;
	private static int points;
	private static int kills;
	private static int deaths;
	public String unlockables;
	
	public String interactiveResponse;
	private boolean interactiveMode = false;
	
	public HexaPlayer(String playername){
		this.setName(playername);
	}
	
	public HexaPlayer(ResultSet rs) throws SQLException{
		this.load(rs);
	}
	
	public void load(ResultSet rs) throws SQLException{
		this.id = rs.getInt("id");
		this.setName(rs.getString("currentname"));
		this.setJoined(rs.getLong("joined"));
		this.setLastOnline(rs.getLong("lastOnline"));
		this.lastIP = rs.getString("last_ip");
		this.setPoints(rs.getInt("points"));
		loadUnlockables(rs.getString("unlocked_kits"));
		
		
		if (rs.getString("uuid").equalsIgnoreCase("broken")) {
			this.uid = null;
		} else {
			this.uid = UUID.fromString(rs.getString("uuid"));
		}
		
		if (!(rs.getString("last_ip").equals(currentIP))){
			this.newIP = true;
		} else {
			this.newIP = false;
		}
	}
	
	private void loadUnlockables(String unlockedstring) {
		if (unlockedstring.equals("none") || unlockedstring.equals("")) {
			return;
		}
		
		String[] unlocked = unlockedstring.split(",");
		
		for (String unlockable : unlocked) {
			if (unlockable != null) {
				this.unlocked.add(unlockable);
			}
		}
	}
	
	private void saveUnlockables() {
		String out = "";
		
		for (String unlockable : this.unlocked) {
			out += unlockable + ",";
		}
		unlockables = out;
	}
	
	//Only need to save anything that could've changed in-game since the last reboot!
	public void SaveNow() {
		//Save Current IP as Last IP
		try {
			MySQL.openConnection();
			PreparedStatement lastipUpdate = MySQL.connection.prepareStatement("UPDATE `Player_Data` SET last_ip=? WHERE currentname=?;");
			//PreparedStatement lastipUpdate = MySQL.connection.prepareStatement("UPDATE `Player_Data` SET last_ip=? WHERE uuid=?;");
			lastipUpdate.setString(1, lastIP);
			//lastipUpdate.setString(2, getUUIDString());
			lastipUpdate.setString(2, getName());
			lastipUpdate.executeUpdate();
			lastipUpdate.close();
		} catch (SQLException e) {
			Log.error("MySQL Exception when updateing last_ip for " + getName() + "!");
			e.printStackTrace();
		} finally {
			MySQL.closeConnection();
		}
		//Save Points
		try {
			MySQL.openConnection();
			PreparedStatement PointsUpdate = MySQL.connection.prepareStatement("UPDATE `Player_Data` SET points=? WHERE currentname=?;");
			//PreparedStatement PointsUpdate = MySQL.connection.prepareStatement("UPDATE `Player_Data` SET points=? WHERE uuid=?;");
			PointsUpdate.setLong(1, points);
			//PointsUpdate.setString(2, getUUIDString());
			PointsUpdate.setString(2, getName());
			PointsUpdate.executeUpdate();
			PointsUpdate.close();
		} catch (SQLException e) {
			Log.error("MySQL Exception when updateing points for " + getName() + "!");
			e.printStackTrace();
		} finally {
			MySQL.closeConnection();
		}
		//Save Kills
		try {
			MySQL.openConnection();
			PreparedStatement PointsUpdate = MySQL.connection.prepareStatement("UPDATE `Player_Data` SET kills=? WHERE currentname=?;");
			//PreparedStatement PointsUpdate = MySQL.connection.prepareStatement("UPDATE `Player_Data` SET kills=? WHERE uuid=?;");
			PointsUpdate.setLong(1, kills);
			PointsUpdate.setString(2, getName());
			//PointsUpdate.setString(2, getUUIDString());
			PointsUpdate.executeUpdate();
			PointsUpdate.close();
		} catch (SQLException e) {
			Log.error("MySQL Exception when updateing kills for " + getName() + "!");
			e.printStackTrace();
		} finally {
			MySQL.closeConnection();
		}
		//Save Deaths
		try {
			MySQL.openConnection();
			PreparedStatement PointsUpdate = MySQL.connection.prepareStatement("UPDATE `Player_Data` SET deaths=? WHERE currentname=?;");
			//PreparedStatement PointsUpdate = MySQL.connection.prepareStatement("UPDATE `Player_Data` SET deaths=? WHERE uuid=?;");
			PointsUpdate.setLong(1, deaths);
			PointsUpdate.setString(2, getName());
			//PointsUpdate.setString(2, getUUIDString());
			PointsUpdate.executeUpdate();
			PointsUpdate.close();
		} catch (SQLException e) {
			Log.error("MySQL Exception when updateing deaths for " + getName() + "!");
			e.printStackTrace();
		} finally {
			MySQL.closeConnection();
		}
		//Save Unlocked Kits
		if(unlocked.isEmpty()){
			return;
		}
		saveUnlockables();
		/*try {
			MySQL.openConnection();
			PreparedStatement UnlockablesUpdate = MySQL.connection.prepareStatement("UPDATE `Player_Data` SET unlocked_kits=? WHERE currentname=?;");
			//PreparedStatement UnlockablesUpdate = MySQL.connection.prepareStatement("UPDATE `Player_Data` SET unlocked_kits=? WHERE uuid=?;");
			UnlockablesUpdate.setString(1, "");
			UnlockablesUpdate.setString(2, getName());
			//UnlockablesUpdate.setString(2, getUUIDString());
			UnlockablesUpdate.executeUpdate();
			UnlockablesUpdate.close();
		} catch (SQLException e) {
			Log.error("MySQL Exception when updateing unlockables for " + getName() + "!");
			e.printStackTrace();
		} finally {
			MySQL.closeConnection();
		}*/
		try {
			MySQL.openConnection();
			PreparedStatement UnlockablesUpdate = MySQL.connection.prepareStatement("UPDATE `Player_Data` SET unlocked_kits=? WHERE currentname=?;");
			//PreparedStatement UnlockablesUpdate = MySQL.connection.prepareStatement("UPDATE `Player_Data` SET unlocked_kits=? WHERE uuid=?;");
			UnlockablesUpdate.setString(1, this.unlockables);
			UnlockablesUpdate.setString(2, getName());
			//UnlockablesUpdate.setString(2, getUUIDString());
			UnlockablesUpdate.executeUpdate();
			UnlockablesUpdate.close();
		} catch (SQLException e) {
			Log.error("MySQL Exception when updateing unlockables for " + getName() + "!");
			e.printStackTrace();
		} finally {
			MySQL.closeConnection();
		}
	}
	
	public String getName(){
		return playername;
	}
	
	public void setName(String name){
		this.playername = name;
	}
	
	public long getJoined() {
		return joined;
	}

	public void setJoined(long joined) {
		this.joined = joined;
	}
	
	public long getLastOnline(){
		return lastOnline;
	}
	
	public void setLastOnline(long lastOnline){
		this.lastOnline = lastOnline;
	}
	
	public int getPoints(){
		return this.points;
	}
	
	public void setPoints(int points){
		this.points = points;
	}
	
	public int getKills(){
		return this.kills;
	}
	
	public void setKills(int kills){
		this.kills = kills;
	}
	
	public int getDeaths(){
		return this.deaths;
	}
	
	public void setDeaths(int deaths){
		this.deaths = deaths;
	}
	
	public String getIP(){
		return this.currentIP;
	}
	
	public void setIP(String ip){
		this.currentIP = ip;
	}
	
	public void setInteractiveMode(String message){
		this.interactiveResponse = message;
	}
	
	public String getInteractiveResponse(){
		return this.interactiveResponse;
	}
	
	public void setInteractiveMode(Boolean mode){
		this.interactiveMode = mode;
	}
	
	public boolean getInteractiveMode(){
		return this.interactiveMode;
	}
	
	public void clearInteractiveMode(){
		this.interactiveMode = false;
		this.interactiveResponse = "";
	}
	
	public Boolean getNewIP(){
		return this.newIP;
	}
	
	public UUID getUUID() {
 		return uid;
 	}
 	
 	//public String getUUIDString() {
 	////	return uid.toString();
 	//}
 	
 	public void setUUID(UUID uid) {
 		this.uid = uid;
 	}
}
