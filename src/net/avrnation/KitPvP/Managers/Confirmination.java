package net.avrnation.KitPvP.Managers;

import java.util.ArrayList;

import net.avrnation.KitPvP.Exceptions;
import net.avrnation.KitPvP.Global;
import net.avrnation.KitPvP.Object.HexaPlayer;
import net.avrnation.KitPvP.Utils.C;

import org.bukkit.entity.Player;

public class Confirmination {

	private static ArrayList<String> confirmination = new ArrayList<String>();
	
	public void AddPlayer(String name) {
		confirmination.add(name.toLowerCase());
	}
	
	public void RemovePlayer(String name) {
		confirmination.remove(name.toLowerCase());
	}
	
	public static boolean CheckPlayer(String name) {
		if(confirmination.contains(name.toLowerCase())) {
			return true;
		}
		return false;
	}
	
	public static void UnlockPurchase(HexaPlayer hexa, String packages, int cost) {
		Player player;
		try {
			player = Global.getPlayer(hexa);
		} catch (Exceptions e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		hexa.setInteractiveMode(true);
		hexa.interactiveResponse = packages;
		player.sendMessage(C.AquaBold + "You currently do not have this Kit Unlocked!");
		player.sendMessage(C.AquaBold + "Type 'yes' if you would be interested in unlocking this Kit for " + cost + " Points!");
		player.sendMessage(C.AquaBold + "Type anything else to cancel this purchase!");
	}
}
