package net.avrnation.KitPvP.Managers;

import net.avrnation.KitPvP.Exceptions;
import net.avrnation.KitPvP.Global;
import net.avrnation.KitPvP.Handlers.Kits.KitManager;
import net.avrnation.KitPvP.Object.HexaPlayer;
import net.avrnation.KitPvP.Utils.C;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class PurchaseUnlockable {

	public static void Respond(String message, HexaPlayer hexa, String unlockable) throws Exceptions {
		
		Player player;
		try {
			player = Global.getPlayer(hexa);
		} catch (Exceptions e) {
			return;
		}
		
		hexa.clearInteractiveMode();
		Bukkit.broadcastMessage("Message Variable returned as " + message);
		if (!(message.equals("yes"))) {
			Global.getPlayer(hexa).sendMessage(C.RedBold + "Purchase Canceled!");
		}
		
		if (KitManager.unlockablecosts.containsKey(unlockable)) {
			if (hexa.getPoints() >= KitManager.unlockablecosts.get(unlockable)) {
				hexa.setPoints(hexa.getPoints() - KitManager.unlockablecosts.get(unlockable));
				hexa.unlocked.add(unlockable);
				Global.getPlayer(hexa).sendMessage(C.GoldBold + "You just purchased an Unlockable for " + KitManager.unlockablecosts.get(unlockable) + "!");
			} else {
				Global.getPlayer(hexa).sendMessage(C.RedBold + "You do not have enough points for this Unlockable!");
			}
		} else {
			Global.getPlayer(hexa).sendMessage(C.RedBold + "This unlockable doesn't seem to exist! Contact a Developer!");
		}
	}
}
